package com.skal.delivery.profile;

public class ProfileupdateRequest {
    String name;
    String email;
    String id;

    public ProfileupdateRequest(String name, String email,String id) {
        this.name = name;
        this.email = email;
        this.id = id;
    }
}
