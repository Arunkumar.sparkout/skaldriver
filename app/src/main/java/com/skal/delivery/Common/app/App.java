package com.skal.delivery.Common.app;

import android.app.Activity;
import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;

import com.skal.delivery.Common.SessionManager;
import com.google.firebase.database.DatabaseReference;
import com.skal.delivery.Common.global.Global;

import java.util.HashMap;
import java.util.Locale;

import static com.skal.delivery.Common.SessionManager.KEY_USER_ID;

public class App extends Application {
    DatabaseReference mDatabaseAvailableProv;
    String userId;
    private static HashMap<String, String> countryCodes;
    @Override
    public void onCreate() {
        Log.e("TAG", "onCreate:App " );
        super.onCreate();
        countryCodes = new Global().setCountryCode();
    }
    public HashMap<String, String> getCountryCodes() {
        return countryCodes;
    }

    @Override
    public void onTerminate() {
        Log.e("TAG", "onTerminate:App");
        super.onTerminate();
    }

    public void changeLang(String lang, Activity activity) {
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);

        Resources resources = activity.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = myLocale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }


}
