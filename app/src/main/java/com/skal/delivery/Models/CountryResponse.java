package com.skal.delivery.Models;

import java.util.List;

public class CountryResponse {
    private String status;
    private List<CountryData> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CountryData> getData() {
        return data;
    }

    public void setData(List<CountryData> data) {
        this.data = data;
    }


}
