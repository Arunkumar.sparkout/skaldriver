package com.skal.delivery.ui.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Models.FoodQuantity;
import com.skal.delivery.Models.RequestDetailPojo;
import com.skal.delivery.ui.ActivityTaskDetail;
import com.skal.delivery.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static io.fabric.sdk.android.Fabric.TAG;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.ViewHolder> {

    List<RequestDetailPojo.FoodDetail> foodList = new ArrayList<>();
    private Context mContext;
    SessionManager sessionManager;
    DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    DecimalFormat decimalFormat = new DecimalFormat("##.00", symbols);

    public FoodListAdapter(ActivityTaskDetail activityTaskDetail, List<RequestDetailPojo.FoodDetail> foodDetailList) {
        this.foodList = foodDetailList;
        mContext = activityTaskDetail;
        sessionManager = new SessionManager(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_food, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RequestDetailPojo.FoodDetail pojo = foodList.get(position);
        Log.e(TAG, "onBindViewHolder:pojo price "+pojo.getPrice() );

        holder.itemNameTxt.setText(pojo.getQuantity() + " X " + pojo.getName());
//        holder.listCartItemAmt.setText(sessionManager.getCurrency()+""+pojo.getPrice());

      /*  Drawable non_veg = mContext.getResources().getDrawable(R.drawable.ic_non_veg);
        Drawable veg = mContext.getResources().getDrawable(R.drawable.ic_veg);

        if (pojo.getIsVeg() == 1) {
            holder.itemNameTxt.setCompoundDrawablesWithIntrinsicBounds(veg, null, null, null);
        } else {
            holder.itemNameTxt.setCompoundDrawablesWithIntrinsicBounds(non_veg, null, null, null);
        }*/

        //------------------------------------
        double itemPrice = 0.0;
        String quantityAddOn = "";
        Log.e(TAG, "onBindViewHolder:getFood_size " + pojo.getFood_size());
        Log.e(TAG, "onBindViewHolder:quantityAddOn before" + quantityAddOn);
        if (foodList.get(position).getFood_size() != null && foodList.get(position).getFood_size().size() > 0) {
            FoodQuantity pojoQuantity = foodList.get(position).getFood_size().get(0);
            quantityAddOn = "Quantity : " + pojoQuantity.getName();
            itemPrice += (pojo.getQuantity() * Double.parseDouble(pojoQuantity.getPrice()));
//            itemPrice += Double.parseDouble(pojoQuantity.getPrice());
            Log.e(TAG, "onBindViewHolder:getFood_size " + quantityAddOn);
            Log.e(TAG, "onBindViewHolder:getFood_size itemPrice " + itemPrice);
            Log.e(TAG, "onBindViewHolder:getFood_size quantityPrice " + pojoQuantity.getPrice());
        } else {
            itemPrice += pojo.getPrice();
        }

        Log.e(TAG, "onBindViewHolder:quantityAddOn after" + quantityAddOn);

        if (pojo.getAdd_ons() != null && pojo.getAdd_ons().size() > 0) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < pojo.getAdd_ons().size(); i++) {
                itemPrice += (pojo.getQuantity() * Double.parseDouble(pojo.getAdd_ons().get(i).getPrice()));
//                itemPrice += Double.parseDouble(pojo.getAdd_ons().get(i).getPrice());
                Log.e(TAG, "onBindViewHolder:getAdd_ons itemPrice " + itemPrice);
                Log.e(TAG, "onBindViewHolder:getAdd_ons Add_onPrice " + pojo.getAdd_ons().get(i).getPrice());
                if (i == 0) {
                    str.append(mContext.getString(R.string.addOns) + pojo.getAdd_ons().get(i).getName());
                    if (pojo.getAdd_ons().size() > 1) {
                        str.append(",");
                    }
                } else if (i == pojo.getAdd_ons().size() - 1) {
                    str.append(pojo.getAdd_ons().get(i).getName());
                } else {
                    str.append(pojo.getAdd_ons().get(i).getName());
                    str.append(",");
                }
            }

            if (quantityAddOn.contains("Quantity")) {
                quantityAddOn = quantityAddOn + "\n" + str.toString();
            } else {
                quantityAddOn = str.toString();
            }


        }

        Log.e(TAG, "onBindViewHolder: quantityAddOn" + quantityAddOn);
        if (quantityAddOn.isEmpty()) {
            holder.itemAddonTxt.setVisibility(View.GONE);
        } else {
            holder.itemAddonTxt.setVisibility(View.VISIBLE);
            holder.itemAddonTxt.setText(quantityAddOn);
        }

        holder.listCartItemAmt.setText(decimalFormat.format(itemPrice)+" "+sessionManager.getCurrency());
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.add_linear)
        LinearLayout addLinear;
        @BindView(R.id.item_name_txt)
        TextView itemNameTxt;
        @BindView(R.id.list_cart_item_amt)
        TextView listCartItemAmt;
        @BindView(R.id.item_addon_txt)
        TextView itemAddonTxt;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
