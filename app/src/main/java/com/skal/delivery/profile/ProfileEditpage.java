package com.skal.delivery.profile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.FilePath;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Config;
import com.skal.delivery.Models.UpdateDriverResponce;
import com.skal.delivery.R;
import com.skal.delivery.SharedPrefsUtils;
import com.skal.delivery.ui.MainActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.skal.delivery.Common.SessionManager.KEY_USER_EMAIL;
import static com.skal.delivery.Common.SessionManager.KEY_USER_ID;
import static com.skal.delivery.Common.SessionManager.KEY_USER_MOBILE;
import static com.skal.delivery.Common.SessionManager.KEY_USER_NAME;

public class ProfileEditpage extends Fragment {
    @BindView(R.id.profile_edit_main)
    CoordinatorLayout profile_edit_main;
    @BindView(R.id.img_profile_edit)
    ImageView img_profile_edit;
    @BindView(R.id.update_name)
    AppCompatEditText update_Name;
    @BindView(R.id.update_email)
    AppCompatEditText update_Email;
    @BindView(R.id.update_phone_number)
    AppCompatTextView update_phone_number;
    @BindView(R.id.update_submit)
    AppCompatTextView update_Submit;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final int CAMERA_REQUEST = 1888;
    Uri imageUri;
    private static int RESULT_LOAD_IMAGE = 1;
    BottomSheetDialog dialog;
    String Username, Useremail;
    MultipartBody.Part body;
    private SessionManager sessionManager;
    String filePath;
    File file;
    List<MultipartBody.Part> imageMap = new ArrayList<>();
    Boolean profilepic = false;
    Uri resultUri;
    private File avatarFile;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profileedit, container, false);
        ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());
        profile_edit_main.setVisibility(View.VISIBLE);


        //phone numbers
        String phone = sessionManager.getUserDetails().get(KEY_USER_MOBILE);
        update_Name.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.username, ""));
        update_phone_number.setText(String.valueOf(phone));
        update_Email.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.useremail, ""));
        img_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    setBottomView();
                } else {
                    requestPermission();
                }
            }
        });
        Log.e("Giri ", "userprofileicon: " + SharedPrefsUtils.getFromPrefs(getActivity(), Config.userprofileicon, ""));
        Glide.with(getActivity())
                .load(SharedPrefsUtils.getFromPrefs(getActivity(), Config.userprofileicon, ""))
                .into(img_profile_edit);

        update_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (update_Name.getText().toString().trim().length() <= 0) {
                    update_Name.setError("Enter valid name");
                    update_Name.findFocus();
                }
                if (!validEmail(update_Email)) {
                    update_Email.setError("Enter valid Email Id");
                    update_Email.findFocus();
                } else {
                    String User_id = sessionManager.getUserDetails().get(KEY_USER_ID);
                    if (profilepic == false) {
                        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                        CommonFunctions.showSimpleProgressDialog(getActivity(), "Updating", false);
                        Call<UpdateDriverResponce> call = apiInterface.updateprofile(sessionManager.getHeader(),
                                (new ProfileupdateRequest(update_Name.getText().toString().trim(), update_Email.getText().toString().trim(), User_id)));
                        call.enqueue(new Callback<UpdateDriverResponce>() {
                            @Override
                            public void onResponse(Call<UpdateDriverResponce> call, Response<UpdateDriverResponce> response) {
                                if (response.isSuccessful()) {
                                    if (response.code() == 200) {
                                        Toast.makeText(getActivity(), "Update profile successfully", Toast.LENGTH_SHORT).show();
                                        sessionManager.getUserDetails().put(KEY_USER_NAME, update_Name.getText().toString());
                                        sessionManager.getUserDetails().put(KEY_USER_EMAIL, update_Email.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.username, update_Name.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.useremail, update_Email.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.userprofileicon, String.valueOf(resultUri));
                                        CommonFunctions.removeProgressDialog();
                                        startActivity(new Intent(getActivity(), MainActivity.class));
                                        getActivity().finish();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), response.code() + "", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateDriverResponce> call, Throwable t) {
                                CommonFunctions.removeProgressDialog();
                            }
                        });

                    } else {

                        HashMap<String, RequestBody> updateMap = new HashMap();
                        HashMap<String, MultipartBody.Part> imageMap = new HashMap();

                        updateMap.put("name", createPartFromString(update_Name.getText().toString().trim()));
                        updateMap.put("email", createPartFromString(update_Email.getText().toString().trim()));
                        updateMap.put("id", createPartFromString(sessionManager.getUserDetails().get(KEY_USER_ID)));

                        if (resultUri != null) {
                            Log.e("TAG", "resultUri: " + resultUri);
                            avatarFile = FilePath.getPath(getActivity(), resultUri);
                            Log.e("TAG", "avatarFile: " + avatarFile);
                        }
                        if (avatarFile != null) {
                            RequestBody requestFile =
                                    RequestBody.create(MediaType.parse("multipart/form-data"), avatarFile);
                            MultipartBody.Part body =
                                    MultipartBody.Part.createFormData(
                                            "profile_image", avatarFile.getName(), requestFile);
                            imageMap.put("profile_image", body);
                        }

                        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                        Call<UpdateDriverResponce> call = apiInterface.updateprofileimage(sessionManager.getHeader(),
                                updateMap, imageMap.get("profile_image"));
                        CommonFunctions.showSimpleProgressDialog(getActivity(), "Updating", false);

                        call.enqueue(new Callback<UpdateDriverResponce>() {
                            @Override
                            public void onResponse(Call<UpdateDriverResponce> call, Response<UpdateDriverResponce> response) {
                                if (response.isSuccessful()) {
                                    if (response.code() == 200) {
                                        Toast.makeText(getActivity(), "Update profile successfully", Toast.LENGTH_SHORT).show();
                                        sessionManager.getUserDetails().put(KEY_USER_NAME, update_Name.getText().toString());
                                        sessionManager.getUserDetails().put(KEY_USER_EMAIL, update_Email.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.username, update_Name.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.useremail, update_Email.getText().toString());
                                        SharedPrefsUtils.saveToPrefs(getActivity(), Config.userprofileicon, String.valueOf(resultUri));
                                        startActivity(new Intent(getActivity(), MainActivity.class));
                                        getActivity().finish();
                                        CommonFunctions.removeProgressDialog();
                                    }

                                } else {
                                    Toast.makeText(getActivity(), response.code() + "", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateDriverResponce> call, Throwable t) {
                                CommonFunctions.removeProgressDialog();
                            }
                        });
                    }
                }
            }
        });
        return view;
    }

    private boolean validEmail(AppCompatEditText update_email) {

        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(update_email.getText().toString()).matches();
    }

    public void setBottomView() {
        View view = getLayoutInflater().inflate(R.layout.include_layout_, null);
        AppCompatTextView cameraAction = view.findViewById(R.id.camera_action);
        AppCompatTextView galleryAction = view.findViewById(R.id.gallery_action);
        cameraAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        galleryAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);
        dialog.show();

    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    public boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            dialog.cancel();
            if (data != null) {
                if (data.getExtras().get("data") != null) {
                    Uri uri = getImageUri(getActivity(), (Bitmap) data.getExtras().get("data"));
                    CropImage.activity(uri).start(getActivity());
                }
            }
        } else if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            dialog.cancel();
            if (data != null) {
                if (data.getData() != null) {
                    CropImage.activity(data.getData()).start(getActivity());
                } else {
                    Toast.makeText(getActivity(), "Unable to select image", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void getCropData(Uri cropResultUri) {
        resultUri = cropResultUri;
        Log.e("TAG", "getCropData: " + resultUri);
        if (resultUri != null) {
            Glide.with(this).load(resultUri)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .dontAnimate())
                    .into(img_profile_edit);
            profilepic = true;
        }
    }

    private String getRealPathFromURI(Uri tempUri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(tempUri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private Uri getImageUri(FragmentActivity activity, Bitmap photo) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), photo, "Title", null);
        return Uri.parse(path);
    }


    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }
}
