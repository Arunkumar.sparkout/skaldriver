package com.skal.delivery.ui.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.global.Global;
import com.skal.delivery.Models.MessageDetailResponse;
import com.skal.delivery.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {


    private Context mContext;
    SessionManager sessionManager;
    DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    DecimalFormat decimalFormat = new DecimalFormat("##.00", symbols);
    List<MessageDetailResponse.MessageData> messageDataList;

    public MessageAdapter(Activity activity, List<MessageDetailResponse.MessageData> messageDataList) {
        mContext = activity;
        sessionManager = new SessionManager(mContext);
        this.messageDataList = messageDataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String time = Global.getDateFromString(messageDataList.get(position).getCreated_at(), "yyyy-MM-dd HH:mm:ss", "EEEE MMM , d hh:mm a");
        holder.txtMesssage.setText(messageDataList.get(position).getMessage());
        holder.txtMessgaTine.setText(time);
    }

    @Override
    public int getItemCount() {
        return messageDataList.size();
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_messsage)
        AppCompatTextView txtMesssage;
        @BindView(R.id.txt_messga_tine)
        AppCompatTextView txtMessgaTine;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
