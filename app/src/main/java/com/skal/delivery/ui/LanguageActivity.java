package com.skal.delivery.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.app.App;
import com.skal.delivery.R;

import static com.skal.delivery.Common.CONST.ENGLISH;
import static com.skal.delivery.Common.CONST.VIETNAMESE;

public class LanguageActivity extends AppCompatActivity {

    RadioButton spanish;
    RadioButton english;
    AppCompatButton ok;
    String language;
    Toolbar toolbar;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        spanish =(RadioButton)findViewById(R.id.vietnam);
        english=(RadioButton)findViewById(R.id.india);
        ok=(AppCompatButton) findViewById(R.id.bn_ok);
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sessionManager = new SessionManager(this);
        setDefaultRadioChecked();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(spanish.isChecked())
                {
                    sessionManager.setCurrentLanguage(VIETNAMESE);
                    new App().changeLang(VIETNAMESE, LanguageActivity.this);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(english.isChecked())
                {
                    sessionManager.setCurrentLanguage("en");
                    new App().changeLang("en", LanguageActivity.this);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void setDefaultRadioChecked() {
        String currentLanguage = sessionManager.getCurrentLanguage();
        if(currentLanguage.equals(ENGLISH)){
            english.setChecked(true);
        }else if(currentLanguage.equals(VIETNAMESE)){
            spanish.setChecked(true);
        }
    }

}
