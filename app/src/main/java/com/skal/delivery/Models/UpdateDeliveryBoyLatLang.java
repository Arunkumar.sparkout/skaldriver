package com.skal.delivery.Models;

import com.skal.delivery.FireBase.FireBaseModels.LatLngModel;

public class UpdateDeliveryBoyLatLang {

    private LatLngModel LatLng;

    public LatLngModel getLatLng() {
        return LatLng;
    }

    public void setLatLng(LatLngModel latLng) {
        LatLng = latLng;
    }
}
