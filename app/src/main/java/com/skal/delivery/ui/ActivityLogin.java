package com.skal.delivery.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.snackbar.Snackbar;
import com.skal.delivery.AndroidBarcodeQrExample;
import com.skal.delivery.Common.CONST;
import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.global.Global;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Config;
import com.skal.delivery.Models.LoginPojo;
import com.skal.delivery.Models.TimeoutSuccessPojo;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.skal.delivery.R;
import com.skal.delivery.SharedPrefsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.*;
import static com.skal.delivery.Common.SessionManager.KEY_USER_NAME;

public class ActivityLogin extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int MY_REQUEST_CODE = 108;
    private static final String TAG = "Login";
    @BindView(R.id.main_login)
    ScrollView main_login;
    @BindView(R.id.login_welcon_txt)
    TextView loginWelconTxt;
    @BindView(R.id.login_mail_edt)
    EditText loginMailEdt;
    @BindView(R.id.login_pswd_edt)
    EditText loginPswdEdt;
    @BindView(R.id.login_txt)
    TextView loginTxt;
    @BindView(R.id.login_forget_pswd_txt)
    TextView loginForgetPswdTxt;
    @BindView(R.id.login_no_acc_txt)
    TextView loginNoAccTxt;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.signup_txt)
    TextView signupTxt;
    @BindView(R.id.txt_terms)
    AppCompatTextView txtTerms;
    private AlertDialog alertDialog;

    APIInterface apiInterface;
    APIClient apiClient;
    SessionManager sessionManager;
    private String refreshedToken;
    private DatabaseReference mDatabaseDeviceToken;
    private FusedLocationProviderClient mFusedLocationClient;
    private String countryCode;
    private GoogleApiClient googleApiClient;
    Location mylocation;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    Snackbar snackbar;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
        setUpGClient();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
        Global.setSpannableText(getString(R.string.login), ActivityLogin.this, txtTerms);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sessionManager = new SessionManager(this);
        mDatabaseDeviceToken = FirebaseDatabase.getInstance().getReference().child(CONST.Params.dev_token);
        getTimeoutLimit();
        checkPermissionmethod();



        if (sessionManager.getDeviceToken() == null) {
            sessionManager.saveDeviceToken(FirebaseInstanceId.getInstance().getToken());
            Log.e(TAG, "onCreate: " + FirebaseInstanceId.getInstance().getToken());
            Log.e(TAG, "onCreate: " + sessionManager.getDeviceToken());
        } else {
            Log.e(TAG, "onCreate: " + sessionManager.getDeviceToken());
        }

       /* loginMailEdt.setText("919600771099");
        loginPswdEdt.setText("12345678");*/


        if (ContextCompat.checkSelfPermission(ActivityLogin.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityLogin.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(ActivityLogin.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                ActivityCompat.requestPermissions(ActivityLogin.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

    }

    private void checkPermissionmethod() {
        if (checkPermission()) {
        } else {
            requestPermission1();
        }
    }

    private void requestPermission1() {
        ActivityCompat.requestPermissions(ActivityLogin.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION},
                PERMISSION_REQUEST_CODE);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                Location mLastLocation = task.getResult();
                                getCurrentCountryCode(mLastLocation);
                            } else {
                                Log.e("TAG", "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e("TAG", "Lost location permission." + unlikely);
        }
    }

    public void getCurrentCountryCode(Location location) {
        //GetCurrentLocation from AgentCurrentLocation Activity's String named as "location"
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (location != null) {

                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    Log.e("Giri ", "run:getCurrentLocation " + latLng);
                    countryCode = Global.getLocationCountryCode(ActivityLogin.this, latLng);
                    Log.e("Giri ", "run:countryCode " + countryCode);
                    if (countryCode != null) {
                        ccp.setCountryForNameCode(countryCode);
                    }
                }
            }

        }, 200);

    }

    @OnClick({R.id.login_txt, R.id.login_forget_pswd_txt, R.id.login_no_acc_txt, R.id.signup_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_txt:

                if (loginMailEdt.getText().toString().isEmpty() || loginMailEdt.getText().length() < 6 || loginPswdEdt.getText().toString().isEmpty()) {
//                    CommonFunctions.shortToast(ActivityLogin.this, getString(R.string.enter_mbl_invalid));
                    snackbar = Snackbar.make(main_login, getString(R.string.enter_mbl_invalid), Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(CONST.Params.phone, ccp.getSelectedCountryCode() + loginMailEdt.getText().toString().trim());
//                    map.put(CONST.Params.phone, loginMailEdt.getText().toString().trim());
                    map.put(CONST.Params.password, loginPswdEdt.getText().toString());
                    map.put(CONST.Params.device_token, sessionManager.getDeviceToken());
                    map.put(CONST.Params.device_type, "android");
                    jsonLogin("login", map, sessionManager.getDeviceToken());
                    Log.e(TAG, "onViewClicked: " + map);
                }

                break;
            case R.id.login_forget_pswd_txt:
                if (loginMailEdt.getText().toString().isEmpty() || loginMailEdt.getText().length() < 6) {
                    CommonFunctions.shortToast(ActivityLogin.this, getString(R.string.enter_valid_mo_no));
                } else {
                    Intent intent = new Intent(ActivityLogin.this, VerifyMobileActivity.class);
                    intent.putExtra(CONST.MOBILE_NUMBER, ccp.getSelectedCountryCode() + loginMailEdt.getText().toString());
//                    intent.putExtra(CONST.MOBILE_NUMBER, loginMailEdt.getText().toString());
                    startActivity(intent);
                }

                break;
            case R.id.login_no_acc_txt:

                alertSignup();

                break;
            case R.id.signup_txt:

                startActivity(new Intent(ActivityLogin.this, RegisterActivity.class));

                break;
        }
    }

    public void alertSignup() {

        // Create a alert dialog builder.
        final AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this, R.style.DialogSlideAnim_leftright);
        // Get custom login form view.
        View view = getLayoutInflater().inflate(R.layout.dialogue_signup, null);
        // Set above view in alert dialog.
        builder.setView(view);

        ImageView signup_close_img = view.findViewById(R.id.signup_close_img);
        EditText signup_phone_edt = view.findViewById(R.id.signup_phone_edt);
        EditText signup_mail_edt = view.findViewById(R.id.signup_mail_edt);
        EditText signup_pswd_edt = view.findViewById(R.id.signup_pswd_edt);
        TextView signup_submit_txt = view.findViewById(R.id.signup_submit_txt);

        signup_close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
        signup_submit_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
                Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                startActivity(intent);
            }
        });

        builder.setCancelable(true);
        alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private void getTimeoutLimit() {
        Call<TimeoutSuccessPojo> call = apiInterface.getProviderTimeout();
        call.enqueue(new Callback<TimeoutSuccessPojo>() {
            @Override
            public void onResponse(Call<TimeoutSuccessPojo> call, Response<TimeoutSuccessPojo> response) {
                if (response.code() == 200) {
                    if (response.body().isStatus()) {
                        sessionManager.setDefaultTimeOut(Integer.parseInt(response.body().getProvider_timeout()));
                    }
                }
            }

            @Override
            public void onFailure(Call<TimeoutSuccessPojo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void jsonLogin(String url, HashMap<String, String> map, String deviceToken) {

        Call<LoginPojo> call = apiInterface.login(url, map, sessionManager.getCurrentLanguage());
        call.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus()) {
                        LoginPojo pojo = response.body();
                        sessionManager.createLoginSession(pojo.getAuthid(), pojo.getAuthtoken(),
                                pojo.getUserName(), pojo.getEmail(), pojo.getPhone(), pojo.getProfileImage(), pojo.getPartner_id(), deviceToken);
                        mDatabaseDeviceToken.child("" + pojo.getAuthid()).setValue(deviceToken);
                       // sessionManager.getUserDetails().get(KEY_USER_NAME)
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(),Config.username,pojo.getUserName());
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(),Config.userprofileicon,pojo.getProfileImage());
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(),Config.user,pojo.getProfileImage());
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(),Config.useremail,pojo.getEmail());
                        SharedPrefsUtils.saveToPrefs(getApplicationContext(), Config.profileoncecall, "profilecallone");

//                        SharedPrefsUtils.saveToPrefs(getApplicationContext(),Config.usernameid,pojo.getEmail());
                        Intent login = new Intent(ActivityLogin.this, MainActivity.class);
                        startActivity(login);
                        finishAffinity();

                    } else
//                        snackbar = Snackbar.make(main_login, response.body().getMessage(), Snackbar.LENGTH_LONG);
//                    snackbar.show();
                        CommonFunctions.shortToast(getApplicationContext(), response.body().getMessage());

                } else {
//                    snackbar = Snackbar.make(main_login, response.body().getMessage(), Snackbar.LENGTH_LONG);
//                    snackbar.show();
                    CommonFunctions.shortToast(getApplicationContext(), response.message());
                }

            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();

    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(ActivityLogin.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);

            }

        }


    }

    private void getMyLocation() {

        if (googleApiClient != null) {

            if (googleApiClient.isConnected()) {

                int permissionLocation = ContextCompat.checkSelfPermission(ActivityLogin.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
//                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
                    PendingResult result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback() {

                        @Override
                        public void onResult(@NonNull Result result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(ActivityLogin.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PERMISSION_GRANTED) {

                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);


                                    } else {
                                        getMyLocation();
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(ActivityLogin.this,
                                                REQUEST_CHECK_SETTINGS_GPS);

                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied. However, we have no way to fix the
                                    // settings so we won't show the dialog.
                                    //finish();
                                    break;
                            }
                        }
                    });
                }

            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // main logic
                } else {
                      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow location permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                checkPermissionmethod();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(ActivityLogin.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

//    private void requestPermission() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && checkSelfPermission(ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED &&
//                    checkSelfPermission(Manifest.permission.RECEIVE_SMS) != PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_SMS) != PERMISSION_GRANTED) {
//                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS,},
//                        MY_REQUEST_CODE);
//            }
//        }
//
//    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
//        new android.app.AlertDialog.Builder(ActivityLogin.this)
//                .setMessage(message)
//                .setPositiveButton("OK", okListener)
//                .setNegativeButton("Cancel", null)
//                .create()
//                .show();
//
//    }
}