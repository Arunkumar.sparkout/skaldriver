package com.skal.delivery.Common;

/**
 * Created by senthil on 23-Feb-18.
 */

public class CONST {

    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
    public static final int SUCCESS_CODE = 200;
    public static final String SERVICE_FAILED = "Service Failed";
    public static final String BUTTON_ACCEPT = "Accept";
    public static final String BUTTON_REJECT = "Reject";
  //  public static final String HELP_URL = "http://157.245.208.68/Skal/cms/restaurant_termsandcondition";
    public static final String FAQ_URL = " http://157.245.208.68/Skal/cms/faq";
    public static final String TERMS_CONDITIONS = "https://www.skalapp.se/copy-of-terms-of-service-1";
    public static final String PRIVACY_POLICY = "https://www.skalapp.se/copy-of-privacy-policy-1";
    public static final String HELP_URL = "http://3.18.202.172/eatzilla/help";
    public static final String REQUEST_ID = "request_id";
    public static final String PAST_ORDER_DETAIL = "past_order_detail";
    public static final String PAST_ORDER_ITEMS = "past_order_items";
    public static final String VEG = "1";
    public static final int DATE_PICKER_REQUEST = 0;
    public static final String ENGLISH = "en" ;
    public static final String SPANISH = "es" ;
    public static final String VIETNAMESE = "vi" ;
    public static final String ONLINE_STATUS = "1";
    public static final String OFFLINE_STATUS = "0";
    public static final String ONRIDE_STATUS = "2";
    public static final int REFRESH_ACTIVITY = 1232;
    public static final int WORK_RIGHTS = 1;
    public static final int DRIVER_LICENSE = 2;
    public static final int CTC_LICENSE = 3;
    public static final int RC_FILE = 4;
    public static final int PROFILE = 0;


    public static final String COUNTRTY = "country";
    public static final String STATE = "state";
    public static final String CITY = "city";
    public static final String AREA = "area";

    //Scanner keys
    public static final int FILLED = 1;
    public static final int BORDER =2;
    public static final String SCANNER_API_KEY = "15861580325IkLPK5Qc0nGdGvD2O9gcvcifHuhBwTbkXAOlpup";
    public static final String scannerCountries ="ad,ae,af,ag,ar,al,am,ao,at,au,az,ba,bb,bd,be,bf,bg,bh," +
            "bi,bj,br,bs,bt,bw,by,bz,ca,cd,cf,cg,ch,cl,cm,cn,co,cr,cu,cy,de,dj," +
            "dk,dm,do,dz,ec,ee,eg,er,es,et,fi,fj,fm,fr,ga,gb,gd,ge,gh,gm,gn,gq,gr," +
            "gt,gy,hk,hn,hr,ht,hu,id,ie,il,is,in,iq,ir,it,jm,jo,jp,ke,kg,kh,ki,km," +
            "kn,kp,kr,kw,kz,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,me,mg,mh,mk,ml,mm," +
            "mn,mr,mt,mu,mv,mw,mx,my,mz,na,ne,ng,ni,no,np,nr,nz,om,pa,pe,pg,ph,pk,pl," +
            "ps,pt,pw,py,qa,ro,rs,ru,rw,sa,sb,sc,sd,se,sg,si,sk,sl,sm,so,sr,ss,st,sv,sy,td," +
            " tg,th,tj,tm,tn,to,tr,tt,tv,tw,tz,ua,ug,us,uy,uz,va,vc,ve,vn,vu,ws,xk,ye,za,zm,zw,nl";

    public static String BASE_URL = "http://157.245.208.68/Skal/api/providerApi/";
    public static String BASE_SEC_URL = "http://157.245.208.68/Skal/api/";
    public static String OCR_URL = "https://accurascan.com/api/v2/";

    public static String APIKEY = "AIzaSyD2y2z0ekJIs-8J7QwZVImThJox82oMoqE";

    public static String currentAddress = "";

    public static final String ORDER_CREATED = "0";
    public static final String RESTAURANT_ACCEPTED = "1";
    public static final String FOOD_PREPARED = "2";
    public static final String DELIVERY_REQUEST_ACCEPTED = "3";
    public static final String REACHED_RESTAURANT = "4";
    public static final String FOOD_COLLECTED_ONWAY = "5";
    public static final String FOOD_DELIVERED = "6";
    public static final String ORDER_COMPLETE = "7";
    public static final String ORDER_BELOW_AGE = "8";
    public static final String ORDER_RETURNED_STORE = "9";
    public static final String ORDER_CANCELLED = "10";


    public static final String NO_ORDER = "-1";
    public static final String BUTTON_REACHED_RESTAURANT = "Reached Store";
    public static final String TOWARDS_CUSTOMER = "Started towards Customer";
    //Renamed for Skal App
    public static final String DELIVERED_TO_CUSTOMER = "Product delivered";
    public static final String ORDER_COMPLETED = "Order Completed";
    public static final String ORDER_RETURNED = "Order Retured";
    public static final String ORDER_BELOW = "Order Retured to store";
//    public static final String DELIVERED_TO_CUSTOMER = "Scan ID";
//    public static final String ORDER_COMPLETED = "Product Delivered";

    public static final String CASH_RECEIVED = "Cash received";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static boolean IS_IN_RIDE;

    public static class Params {

        public static String phone = "phone";
        public static String device_token = "device_token";
        public static String password = "password";
        public static String email = "email";

        public static String available_providers = "available_providers";
        public static String providers_status = "providers_status";

        public static String lat = "lat";
        public static String lng = "lng";
        public static String new_request = "new_request";
        public static String current_request = "current_request";
        public static String request_id = "request_id";
        public static String provider_id = "provider_id";
        public static String status = "status";
        public static String LatLng = "LatLng";
        public static String prov_location = "prov_location";
        public static String dev_token = "log_device_token";
        public static String device_type = "device_type";
        public static String login_type = "login_type";
        public static String user_id = "user_id";
    }

    public static class Keywords {

        //these keywords used in login page to make different visibility of linear inside card
        public static final String sign_in = "signIn";//To display Signin page
        public static final String sign_up = "signUp";//To display Register page
        public static final String verification = "verification";//To show otp verification
        public static final String forgot_pswd = "forgotPswd";// To show forgot password geting phone number screen
        public static final String new_pswd = "newPswd";//To show reset password page


    }
}
