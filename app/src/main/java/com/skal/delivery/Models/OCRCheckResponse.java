package com.skal.delivery.Models;

public class OCRCheckResponse {
    private String Status;
    private String Message;
    private Scandata data;

    public Scandata getData() {
        return data;
    }

    public void setData(Scandata data) {
        this.data = data;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public class Scandata {
       private OCRdata ocrdata;
       private MRZdat MRZdata;

        public MRZdat getMRZdata() {
            return MRZdata;
        }

        public void setMRZdata(MRZdat MRZdata) {
            this.MRZdata = MRZdata;
        }

        public OCRdata getOcrdata() {
            return ocrdata;
        }

        public void setOcrdata(OCRdata ocrdata) {
            this.ocrdata = ocrdata;
        }
    }
}
