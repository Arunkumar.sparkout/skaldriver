package com.skal.delivery;

public class Config {
    public static final String KEY_QR_CODE ="KEY_QR_CODE" ;
    public static String username="username";
    public static String user="user";
    public static String phonenumber="phonenumber";
    public static String usernameprofile="usernameprofile";
    public static String usernameid="usernameid";
    public static String userlocationprofile="userlocationprofile";
    public static String useraddressprofile="useraddressprofile";
    public static String userratingprofile="userratingprofile";
    public static String userjoiningprofile="joiningdate";
    public static String userservicezone="servicezone";
    public static String userprofileicon ="userprofileicon";
    public static String useremail="useremail";
    public static String profileoncecall="profileoncecall";
}
