package com.skal.delivery.Common;

import android.util.Log;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.content.ContentValues.TAG;


public class WebUtils {

  public static MultipartBody.Part getImagePart(String key, File file) {
    try {
      Log.e(TAG, "getImagePart:file "+file );
      RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
      Log.e("File"," "+file);
      return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
    } catch (Exception e) {
      Log.e(TAG, "getImagePart:Exception "+e );
      e.printStackTrace();
    }
    return null;
  }

  public static RequestBody createRequest(String value) {
    if (value != null) {
      Log.e("Not null","success "+value);
      try {
        return RequestBody.create(MediaType.parse("text/plain"), value);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
      return null;
    }

}
