package com.skal.delivery.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.EventModels.LogoutEvent;
import com.skal.delivery.Models.MessageDetailResponse;
import com.skal.delivery.Models.ProfileDetailsResponse;
import com.skal.delivery.R;
import com.skal.delivery.ui.Adapter.MessageAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_messages)
    RecyclerView recyclerMessages;
    MessageAdapter messageAdapter;
    private APIInterface apiInterface;
    private SessionManager sessionManager;
    private HashMap<String, String> userDetails;
    private List<MessageDetailResponse.MessageData> messageDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sessionManager = new SessionManager(this);
        userDetails = sessionManager.getUserDetails();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerMessages.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(MessageActivity.this, messageDataList);
        recyclerMessages.setAdapter(messageAdapter);
        getMessageDetails();
    }

    private void getMessageDetails() {
        Call<MessageDetailResponse> call = apiInterface.getMessages(sessionManager.getHeader(), sessionManager.getCurrentLanguage());
        call.enqueue(new Callback<MessageDetailResponse>() {
            @Override
            public void onResponse(Call<MessageDetailResponse> call, Response<MessageDetailResponse> response) {
                if (response.code() == 200) {
                    if (response.body().isStatus()) {
                        if (!messageDataList.isEmpty()) {
                            messageDataList.clear();
                        }

                        messageDataList.addAll(response.body().getData());
                        messageAdapter.notifyDataChanged();
                        if (response.body().getData().isEmpty()) {
                            CommonFunctions.shortToast(getApplicationContext(), "No Messages Found");
                        }
                    }
                } else if (response.code() == 401) {
                    sessionManager.logoutUser(MessageActivity.this);
                    CommonFunctions.shortToast(getApplicationContext(), response.message());
                }
            }

            @Override
            public void onFailure(Call<MessageDetailResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUnauthorise(LogoutEvent logoutEvent) {
        Log.e("TAG", "onUnauthorise: Event");
        new SessionManager(this).logoutUser(this);
    }

}
