package com.skal.delivery.ui.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.skal.delivery.Common.callbacks.OnButtonClick;
import com.skal.delivery.EventModels.LogoutEvent;
import com.skal.delivery.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.skal.delivery.Common.CONST.BORDER;
import static com.skal.delivery.Common.CONST.FILLED;
import static com.skal.delivery.Common.global.Global.getCodeName;

public class ButtonListAdapter extends RecyclerView.Adapter {

    Activity activity;
    List<String> codes;
    OnButtonClick onButtonClick;
    long previousNumber = 0;

    public ButtonListAdapter(Activity activity, List<String> codes, OnButtonClick onButtonClick) {
        this.activity = activity;
        this.codes = codes;
        this.onButtonClick = onButtonClick;
        Log.e("TAG", "ButtonListAdapter:codes "+codes.size() );
    }

    @Override
    public int getItemViewType(int position) {
        int value = 0;
        if (position%2 == 0) {
            value = 1;
        }
        Log.e("TAG", "getItemViewType:value "+value );

        switch (value) {
            case 0:
                return BORDER;
            case 1:
                return FILLED;
            default:
                return 0;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case FILLED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filled, parent, false);
                return new FilledViewHolder(view);
            case BORDER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_border, parent, false);
                return new BorderViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == FILLED) {
            ((FilledViewHolder) holder).btnnfilled.setText(getCodeName(codes.get(position)));
        } else {
            ((BorderViewHolder) holder).btnnBorder.setText(getCodeName(codes.get(position)));
        }
    }

    @Override
    public int getItemCount() {
        return codes.size();
    }

    public class FilledViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnn_aadhar)
        AppCompatButton btnnfilled;

        public FilledViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            btnnfilled.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(activity, "hai", Toast.LENGTH_SHORT).show();

                    onButtonClick.getCode(codes.get(getAbsoluteAdapterPosition()));
                }
            });
        }
    }



    public class BorderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnn_pan)
        AppCompatButton btnnBorder;

        public BorderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            btnnBorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClick.getCode(codes.get(getAbsoluteAdapterPosition()));
                   // Toast.makeText(activity, "hai", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
