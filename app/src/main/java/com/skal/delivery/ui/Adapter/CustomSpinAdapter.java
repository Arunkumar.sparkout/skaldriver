package com.skal.delivery.ui.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.skal.delivery.Common.CONST;
import com.skal.delivery.Models.CountryData;
import com.skal.delivery.R;

import java.util.ArrayList;
import java.util.List;

public class CustomSpinAdapter extends BaseAdapter {
    Activity activity;
    LayoutInflater inflater;
    List<CountryData> data;
    String type;

    public CustomSpinAdapter(Activity activity, List<CountryData> data, String type) {
        this.activity = activity;
        this.data = data;
        this.type = type;
        inflater = (LayoutInflater.from(activity));
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        switch (type) {
            case CONST.COUNTRTY:
                return data.get(i).getCountry_name();
            case CONST.STATE:
                return data.get(i).getState_name();
            case CONST.CITY:
                return data.get(i).getCity_name();
            case CONST.AREA:
                return data.get(i).getArea();
            default:
                return null;
        }
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.custom_spin_layout, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        switch (type) {
            case CONST.COUNTRTY:
                names.setText(data.get(position).getCountry_name());
                break;
            case CONST.STATE:
                names.setText(data.get(position).getCity_name());
                break;
            case CONST.CITY:
                names.setText(data.get(position).getCity_name());
                break;
            case CONST.AREA:
                names.setText(data.get(position).getArea());
                break;
        }
        return view;
    }
}