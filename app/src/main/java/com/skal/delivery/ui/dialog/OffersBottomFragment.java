package com.skal.delivery.ui.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hbb20.CountryCodePicker;
import com.skal.delivery.AndroidBarcodeQrExample;
import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.ImageFilePath;
import com.skal.delivery.Common.WebUtils;
import com.skal.delivery.Common.app.App;
import com.skal.delivery.Common.callbacks.OnButtonClick;
import com.skal.delivery.Common.callbacks.onImageVerifySuccess;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Models.OCRCheckResponse;
import com.skal.delivery.R;
import com.skal.delivery.ui.Adapter.ButtonListAdapter;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.skal.delivery.Common.CONST.SCANNER_API_KEY;
import static com.skal.delivery.Common.CONST.scannerCountries;
import static com.skal.delivery.Common.global.Global.getCodes;

public class OffersBottomFragment extends BottomSheetDialogFragment implements OnButtonClick {
    View view;
    Unbinder unbinder;
    Activity activity;
    onImageVerifySuccess onImageVerify;
    @BindView(R.id.mainactivty)
    LinearLayout mainActivity;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    HashMap<String, String> countryCodes;
    String countryCode;
    String docType;
    @BindView(R.id.img_doc)
    AppCompatImageView imgDoc;
    @BindView(R.id.scan_icon)
    AppCompatTextView Scanicon;
    @BindView(R.id.order_return)
    AppCompatTextView orderreturn;
    private APIInterface apiInterface;
    private static final int CROP_IMAGE_ACTIVITY_REQUEST_CODE = 200;
    ButtonListAdapter buttonListAdapter;
    private static final int REQUEST_CODE_QR_SCAN = 101;
//    BottomSheetBehavior behavior;


    public OffersBottomFragment(onImageVerifySuccess onImageVerify) {
        this.onImageVerify = onImageVerify;
        countryCodes = new App().getCountryCodes();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ocrr_dialog, container, false);

        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        apiInterface = APIClient.getOCRClient().create(APIInterface.class);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
        recycler.setLayoutManager(linearLayoutManager);


        //country list is set based on scanner availability
        ccp.setCustomMasterCountries(scannerCountries);
        setValues();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                setValues();
            }
        });

        Scanicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AndroidBarcodeQrExample.class));

            }
        });

        imgDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
//                    Intent i = new Intent(getActivity(), QrCodeActivity.class);
//                    startActivityForResult( i,REQUEST_CODE_QR_SCAN);

//
//                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(cameraIntent, CROP_IMAGE_ACTIVITY_REQUEST_CODE);
                } else {
                    requestPermission();
                }
            }
        });

        return view;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA},
                CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void setValues() {
        countryCode = countryCodes.get(ccp.getSelectedCountryNameCode().toLowerCase()).toUpperCase();
        Log.e("TAG", "onCountrySelected:countryCode " + countryCode);
        ButtonListAdapter buttonListAdapter = new ButtonListAdapter(activity, getCodes(countryCode), this::getCode);
        recycler.setAdapter(buttonListAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getCode(String code) {
        docType = code;
        Log.e("TAG", "getCode:docType " + docType);
        CropImage.activity()
                .setCropMenuCropButtonTitle("SELECT")
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(activity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TAG", "onActivityResult: OffersBottomFragment");


        if (requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imgDoc.setScaleType(AppCompatImageView.ScaleType.FIT_XY);
            imgDoc.setImageBitmap(photo);

            //Uri tempUri = getImageUri(getActivity(), photo);
            //   File file = new File(getRealPathFromURI(tempUri));
            //  MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));


            // Log.e("filePath ", "" + tempUri);
//            Log.e("filePath ", "" + file);
//            Log.e("filePath ", "" + filePart);
            //  mainActivity.setVisibility(View.GONE);

//            behavior = BottomSheetBehavior.from(mainActivity);
//            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);


            //updateProfile(id, fullName, body, other);
            onImageVerify.onVerifySuccess();
            //docVerifyApi(imgDoc);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String filePath = ImageFilePath.getPath(activity, resultUri);
                Log.e("filePath ", "" + filePath);
                Log.e("filePath ", "" + resultUri);
                Log.e("filePath ", "" + result);

                imgDoc.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgDoc);

                onImageVerify.onVerifySuccess();
            }
//                // docVerifyApi(imgDoc);
//        else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//
//            CommonFunctions.shortToast(activity, "Error Upload: " + error);
//        }


        }
    }

    private String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    private Uri getImageUri(FragmentActivity activity, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);

    }


    private void docVerifyApi(AppCompatImageView imgDoc) {
        CommonFunctions.showSimpleProgressDialog(activity, "Verifying Doc", false);
        HashMap<String, RequestBody> map = new HashMap<>();
        List<MultipartBody.Part> imageMap = new ArrayList<>();
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Api-Key", SCANNER_API_KEY);
        map.put("country_code", WebUtils.createRequest(countryCode));
        map.put("card_code", WebUtils.createRequest(docType));
        imageMap.add(WebUtils.getImagePart("scan_image", new File(imgDoc.getTag().toString())));
        // imageMap.add(WebUtils.getImagePart("scan_image", new File(String.valueOf(imgDoc))));
        Log.e("TAG", "docVerifyApi: map" + map);
        Log.e("TAG", "docVerifyApi:imageMap" + imageMap);
        Log.e("TAG", "docVerifyApi: header" + header);

        Call<OCRCheckResponse> call = apiInterface.verifyOCRImage(header, map, imageMap);
        call.enqueue(new Callback<OCRCheckResponse>() {
            @Override
            public void onResponse(Call<OCRCheckResponse> call, Response<OCRCheckResponse> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        dismiss();
                        if (docType.equalsIgnoreCase("MRZ")) {
                            if (response.body().getData().getMRZdata() != null) {
                                onImageVerify.onVerifySuccess();
                            } else {
                                Toast.makeText(getContext(), "Invalid Verification ID", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            if (response.body().getData().getOcrdata() != null) {
                                onImageVerify.onVerifySuccess();
                            } else {
                                Toast.makeText(getContext(), "Invalid Verification ID", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                CommonFunctions.removeProgressDialog();
            }

            @Override
            public void onFailure(Call<OCRCheckResponse> call, Throwable t) {
                CommonFunctions.removeProgressDialog();
                Toast.makeText(getContext(), "Verification Failed", Toast.LENGTH_LONG).show();
            }
        });

    }
}