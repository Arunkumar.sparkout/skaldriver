package com.skal.delivery.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.hbb20.CountryCodePicker;
import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.DatePickerActivFragment;
import com.skal.delivery.Common.ImageFilePath;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.WebUtils;
import com.skal.delivery.Common.global.Global;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Models.CountryData;
import com.skal.delivery.Models.CountryResponse;
import com.skal.delivery.Models.DriverRegisterResponse;
import com.skal.delivery.R;
import com.skal.delivery.ui.Adapter.CustomSpinAdapter;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skal.delivery.Common.CONST.AREA;
import static com.skal.delivery.Common.CONST.CITY;
import static com.skal.delivery.Common.CONST.COUNTRTY;
import static com.skal.delivery.Common.CONST.CTC_LICENSE;
import static com.skal.delivery.Common.CONST.DRIVER_LICENSE;
import static com.skal.delivery.Common.CONST.PROFILE;
import static com.skal.delivery.Common.CONST.RC_FILE;
import static com.skal.delivery.Common.CONST.STATE;
import static com.skal.delivery.Common.CONST.WORK_RIGHTS;

public class RegisterActivity extends AppCompatActivity implements DatePickerActivFragment.OnDateCompleteListener, Spinner.OnItemSelectedListener {


    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.edt_driver_name)
    AppCompatEditText edtDriverName;
    @BindView(R.id.edt_driver_password)
    AppCompatEditText edtDriverPassword;
    @BindView(R.id.edt_driver_email)
    AppCompatEditText edtDriverEmail;
    @BindView(R.id.edt_driver_mobile)
    AppCompatEditText edtDriverMobile;
    @BindView(R.id.edt_vehicle_model)
    AppCompatEditText edtVehicleModel;
    @BindView(R.id.edt_vehicle_no)
    AppCompatEditText edtVehicleNo;
    @BindView(R.id.edt_address)
    AppCompatEditText edtAddress;
    @BindView(R.id.txt_person_number)
    AppCompatEditText txt_person_number;
    @BindView(R.id.edt_area)
    AppCompatEditText edt_area;
    @BindView(R.id.spin_area)
    Spinner spinArea;
    @BindView(R.id.spin_city)
    Spinner spinCity;
    @BindView(R.id.spin_country)
    Spinner spinCountry;
    @BindView(R.id.spin_state_province)
    Spinner spinStateProvince;
    @BindView(R.id.edt_zipcode)
    AppCompatEditText edtZipcode;
    @BindView(R.id.edt_reg_no)
    AppCompatEditText edtRegNo;
    @BindView(R.id.edt_ctp_no)
    AppCompatEditText edtCtpNo;
    @BindView(R.id.lay1)
    LinearLayout lay1;
    @BindView(R.id.spin_doc_type)
    Spinner spinDocType;
    @BindView(R.id.lay2)
    LinearLayout lay2;
    @BindView(R.id.txt_license_expiry)
    AppCompatTextView txtLicenseExpiry;
    @BindView(R.id.lay3)
    LinearLayout lay3;
    @BindView(R.id.txt_ctc_expiry)
    AppCompatTextView txtCtcExpiry;
    @BindView(R.id.lay4)
    LinearLayout lay4;
    @BindView(R.id.btn_next)
    AppCompatButton btnNext;
    DialogFragment dateDialogFragment;
    @BindView(R.id.img_work_right)
    AppCompatImageView imgWorkRight;
    @BindView(R.id.img_driver_license)
    AppCompatImageView imgDriverLicense;
    @BindView(R.id.img_ctc)
    AppCompatImageView imgCtc;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_work_right_expiry)
    AppCompatTextView txtWorkRightExpiry;
    @BindView(R.id.img_rc)
    AppCompatImageView imgRc;
    @BindView(R.id.txt_rc_expiry)
    AppCompatTextView txtRcExpiry;
    @BindView(R.id.lay5)
    LinearLayout lay5;
    @BindView(R.id.txt_terms)
    AppCompatTextView txtTerms;
    @BindView(R.id.spin_deliv_mode)
    Spinner spinDelivMode;
    @BindView(R.id.lay_doc_info)
    LinearLayout layDocInfo;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.document_layout)
    LinearLayout documentLayout;
    @BindView(R.id.txt_brand)
    AppCompatTextView txtBrand;
    @BindView(R.id.txt_model)
    AppCompatTextView txtModel;
    private int selectedView;
    private APIInterface apiInterface;
    List<CountryData> countryResponseList = new ArrayList<>();
    List<CountryData> cityResponseList = new ArrayList<>();
    List<CountryData> stateResponseList = new ArrayList<>();
    List<CountryData> areaResponseList = new ArrayList<>();
    CustomSpinAdapter countryAdapter;
    CustomSpinAdapter stateAdapter;
    CustomSpinAdapter cityAdapter;
    CustomSpinAdapter areaAdapter;
    CountryData defaultData;
    String[] countryNames = {"Passport", "ID Card"};
    String[] DeliveryTypes = {"Cycle", "Bike", "Car"};
    private SessionManager sessionManager;
    private int selectedDeliveryMode = 1;
    private String countryCode;
    private FusedLocationProviderClient mFusedLocationClient;
    String manufacturer, model;
    String countryname;
    Boolean countryrequest = false;
    String CountryID;
    TelephonyManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ButterKnife.bind(this);

        countryname = getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry();
        manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //   ccp.setCountryForNameCode(CountryID);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiInterface = APIClient.getBaseClient().create(APIInterface.class);

        sessionManager = new SessionManager(this);
        manufacturer = sessionManager.capitalize(Build.MANUFACTURER);
        model = Build.MODEL;
        txtBrand.setText(manufacturer);
        txtModel.setText(model);

        defaultData = new CountryData();
        //  spinCountry.setSelection(countryname.);
        //if (countryname.equalsIgnoreCase(arrayList.get(p)))
        defaultData.setCountry_name("Select Country");
        defaultData.setCity_name("Select City");
        defaultData.setState_name("Select City");
        defaultData.setArea("Select Apt/Suite");
        countryResponseList.add(defaultData);


        spinCountry.setOnItemSelectedListener(this);
        spinCity.setOnItemSelectedListener(this);
        spinStateProvince.setOnItemSelectedListener(this);
        spinArea.setOnItemSelectedListener(this);
        spinDocType.setOnItemSelectedListener(this);
        spinDelivMode.setOnItemSelectedListener(this);

        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        countryAdapter = new CustomSpinAdapter(RegisterActivity.this, countryResponseList, COUNTRTY);
        spinCountry.setAdapter(countryAdapter);

        stateAdapter = new CustomSpinAdapter(RegisterActivity.this, stateResponseList, CITY);
        spinStateProvince.setAdapter(stateAdapter);

        cityAdapter = new CustomSpinAdapter(RegisterActivity.this, cityResponseList, CITY);
        spinCity.setAdapter(cityAdapter);

        areaAdapter = new CustomSpinAdapter(RegisterActivity.this, areaResponseList, AREA);
        spinArea.setAdapter(areaAdapter);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinDocType.setAdapter(adapter);
//        spinDocType.setSelection(0);

        ArrayAdapter<String> Delivadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, DeliveryTypes);
        Delivadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinDelivMode.setAdapter(Delivadapter);


        setDefaults(COUNTRTY);

    }


    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                Location mLastLocation = task.getResult();
                                getCurrentCountryCode(mLastLocation);
                            } else {
                                Log.e("TAG", "Failed to get location.");
                            }
                        }
                    });
        } catch (SecurityException unlikely) {
            Log.e("TAG", "Lost location permission." + unlikely);
        }
    }

    public void getCurrentCountryCode(Location location) {
        //GetCurrentLocation from AgentCurrentLocation Activity's String named as "location"
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (location != null) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    Log.e("Giri ", "run:getCurrentLocation " + latLng);
                    countryCode = Global.getLocationCountryCode(RegisterActivity.this, latLng);
                    Log.e("Giri ", "run:countryCode " + countryCode);
                    if (countryCode != null) {
                        ccp.setCountryForNameCode(countryCode);
                    } else {
                        CountryID = manager.getSimCountryIso().toUpperCase();
                    }
                }
            }
        }, 200);
    }

    private void setDefaults(String type) {
        Call<CountryResponse> call = null;
        switch (type) {
            case COUNTRTY:
                call = apiInterface.getCountryList();
                break;
            case STATE:
//                call = apiInterface.getStateList(countryResponseList.get(getIdPosition(type)).getCountry_id());
                call = apiInterface.getCityList(countryResponseList.get(spinCountry.getSelectedItemPosition()).getCountry_id());
                break;
            case CITY:
//                call = apiInterface.getCityList(stateResponseList.get(getIdPosition(type)).getState_id());
                call = apiInterface.getCityList(stateResponseList.get(spinStateProvince.getSelectedItemPosition()).getCountry_id());
                break;
            case AREA:
//                call = apiInterface.getAreaList(areaResponseList.get(getIdPosition(type)).getCity_id());
                call = apiInterface.getAreaList(cityResponseList.get(spinCity.getSelectedItemPosition()).getCity_id());
                break;
        }
        call.enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                if (response.code() == 200) {
                    /* if (!response.body().getData().isEmpty()) {*/
                    Log.e("giri", "onResponse: " + response);
                    setAdapterValues(type, response.body().getData());
                    if (countryrequest == false) {
                        spinCountry.setSelection(getCountryPosition(countryname));
                        countryrequest = true;
                    }
                    cityAdapter.notifyDataSetChanged();
                    stateAdapter.notifyDataSetChanged();
                        /*ArrayAdapter<CountryData> adapter =
                               new ArrayAdapter<>(RegisterActivity.this,  android.R.layout.simple_spinner_dropdown_item, countryResponseList);
                        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                        spinCountry.setAdapter(adapter);*/
                }
            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {

            }
        });
    }

    /*private int getIdPosition(String type) {
        switch (type) {
            case STATE:
                return spinCountry.getSelectedItemPosition() - 1 < 0 ? 0 : spinCountry.getSelectedItemPosition() - 1;
            case CITY:
                return spinStateProvince.getSelectedItemPosition() - 1 < 0 ? 0 : spinStateProvince.getSelectedItemPosition() - 1;
            case AREA:
                return spinCity.getSelectedItemPosition() - 1 < 0 ? 0 : spinCity.getSelectedItemPosition() - 1;
        }
        return 0;
    }*/

    private void setAdapterValues(String type, List<CountryData> data) {
        switch (type) {
            case COUNTRTY:
                Log.e("giri", "onResponse: " + countryResponseList.size());
                countryResponseList.addAll(data);
                stateResponseList.clear();
                stateResponseList.add(defaultData);
                cityResponseList.clear();
                cityResponseList.add(defaultData);
                areaResponseList.clear();
                areaResponseList.add(defaultData);
                stateAdapter.notifyDataSetChanged();
                cityAdapter.notifyDataSetChanged();
                areaAdapter.notifyDataSetChanged();
                countryAdapter.notifyDataSetChanged();
                spinStateProvince.setSelection(0);
                spinCity.setSelection(0);
                spinArea.setSelection(0);
                break;
            case STATE:
                stateResponseList.clear();
                stateResponseList.add(defaultData);
                stateResponseList.addAll(data);
                cityResponseList.clear();
                cityResponseList.add(defaultData);
                areaResponseList.clear();
                areaResponseList.add(defaultData);
                stateAdapter.notifyDataSetChanged();
                cityAdapter.notifyDataSetChanged();
                areaAdapter.notifyDataSetChanged();
                spinStateProvince.setSelection(0);
                spinCity.setSelection(0);
                spinArea.setSelection(0);
                break;
            case CITY:
                cityResponseList.clear();
                cityResponseList.add(defaultData);
                cityResponseList.addAll(data);
                areaResponseList.clear();
                areaResponseList.add(defaultData);
                cityAdapter.notifyDataSetChanged();
                areaAdapter.notifyDataSetChanged();
                spinCity.setSelection(0);
                spinArea.setSelection(0);
                break;
            case AREA:
                areaResponseList.clear();
                areaResponseList.add(defaultData);
                areaResponseList.addAll(data);
                areaAdapter.notifyDataSetChanged();
                spinArea.setSelection(0);
                break;
        }

    }

    @OnClick({R.id.txt_license_expiry, R.id.txt_ctc_expiry, R.id.btn_next, R.id.img_work_right, R.id.img_driver_license,
            R.id.img_ctc, R.id.txt_work_right_expiry, R.id.txt_rc_expiry, R.id.img_rc, R.id.img_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_license_expiry:
                dateDialogFragment = new DatePickerActivFragment();
                dateDialogFragment.show(getSupportFragmentManager(), "License Picker");
                break;
            case R.id.txt_ctc_expiry:
                dateDialogFragment = new DatePickerActivFragment();
                dateDialogFragment.show(getSupportFragmentManager(), "CTC Picker");
                break;
            case R.id.txt_work_right_expiry:
                dateDialogFragment = new DatePickerActivFragment();
                dateDialogFragment.show(getSupportFragmentManager(), "Work Picker");
                break;
            case R.id.txt_rc_expiry:
                dateDialogFragment = new DatePickerActivFragment();
                dateDialogFragment.show(getSupportFragmentManager(), "RC Picker");
                break;
            case R.id.btn_next:
                setNextClickVisibility();
                break;
            case R.id.img_work_right:
                selectedView = WORK_RIGHTS;
                CropImage.activity()
                        .setCropMenuCropButtonTitle("SELECT")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.img_driver_license:
                selectedView = DRIVER_LICENSE;
                CropImage.activity()
                        .setCropMenuCropButtonTitle("SELECT")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.img_ctc:
                selectedView = CTC_LICENSE;
                CropImage.activity()
                        .setCropMenuCropButtonTitle("SELECT")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.img_rc:
                selectedView = RC_FILE;
                CropImage.activity()
                        .setCropMenuCropButtonTitle("SELECT")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
            case R.id.img_profile:
                selectedView = PROFILE;
                CropImage.activity()
                        .setCropMenuCropButtonTitle("SELECT")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                String filePath = ImageFilePath.getPath(RegisterActivity.this, resultUri);

                Log.e("filePath ", "" + filePath);
                setImage(filePath, resultUri);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                CommonFunctions.shortToast(RegisterActivity.this, "Error Upload: " + error);
            }
        }
    }

    private void setImage(String filePath, Uri resultUri) {
        switch (selectedView) {
            case PROFILE:
                imgProfile.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgProfile);
                break;
            case WORK_RIGHTS:
                imgWorkRight.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgWorkRight);
                break;
            case DRIVER_LICENSE:
                imgDriverLicense.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgDriverLicense);
                break;
            case CTC_LICENSE:
                imgCtc.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgCtc);
                break;
            case RC_FILE:
                imgRc.setTag(filePath);
                Picasso.get()
                        .load(resultUri)
                        .fit()
                        .into(imgRc);
                break;
        }
    }

/*
    private void setNextClickVisibility() {
        if (lay1.getVisibility() == View.VISIBLE && fieldValidate()) {
            hideKeyboard(this);
            progressBar.setProgress(25);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.VISIBLE);
            lay1.setVisibility(View.GONE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
            if (layDocInfo.getVisibility() == View.VISIBLE) {
                txtTerms.setVisibility(View.VISIBLE);
                Global.setSpannableText("Submit", RegisterActivity.this, txtTerms);
                btnNext.setText("Submit");
            } else {
                txtTerms.setVisibility(View.GONE);
                btnNext.setText("Next");
            }
        } else if (lay2.getVisibility() == View.VISIBLE && workRightValidate()) {
            hideKeyboard(this);
            if (layDocInfo.getVisibility() == View.VISIBLE) {
                progressBar.setProgress(50);
                lay3.setVisibility(View.VISIBLE);
                lay2.setVisibility(View.GONE);
                lay1.setVisibility(View.GONE);
                lay4.setVisibility(View.GONE);
                lay5.setVisibility(View.GONE);
            } else {
                progressBar.setProgress(100);
                apiRegister();
            }
        } else if (lay3.getVisibility() == View.VISIBLE && licenseValidate()) {
            hideKeyboard(this);
            if (layDocInfo.getVisibility() == View.VISIBLE) {
                progressBar.setProgress(75);
                lay4.setVisibility(View.VISIBLE);
                lay2.setVisibility(View.GONE);
                lay3.setVisibility(View.GONE);
                lay1.setVisibility(View.GONE);
                lay5.setVisibility(View.GONE);
            } else {
                progressBar.setProgress(100);
                apiRegister();
            }
        } else if (lay4.getVisibility() == View.VISIBLE && ctcValidate()) {
            hideKeyboard(this);
            progressBar.setProgress(90);
            lay4.setVisibility(View.GONE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.GONE);
            lay1.setVisibility(View.GONE);
            lay5.setVisibility(View.VISIBLE);
            txtTerms.setVisibility(View.VISIBLE);
            Global.setSpannableText("Submit", RegisterActivity.this, txtTerms);
            btnNext.setText("Submit");
        } else if (lay5.getVisibility() == View.VISIBLE && rcValidate()) {
            hideKeyboard(this);
            apiRegister();
        }
    }
*/

    private void setNextClickVisibility() {
        if (documentLayout.getVisibility() == View.VISIBLE) {
            if (fieldValidate() && workRightValidate() && licenseValidate() /*&& ctcValidate() && rcValidate()*/) {
                hideKeyboard(this);
                if (txtTerms.getVisibility() == View.GONE) {
                    txtTerms.setVisibility(View.VISIBLE);
                    Global.setSpannableText(getString(R.string.submit), RegisterActivity.this, txtTerms);
                } else {
                    apiRegister();
                }
            }
        } else if (fieldValidate() && workRightValidate()) {
            hideKeyboard(this);
            if (txtTerms.getVisibility() == View.GONE) {
                txtTerms.setVisibility(View.VISIBLE);
                Global.setSpannableText(getString(R.string.submit), RegisterActivity.this, txtTerms);
            } else {
                apiRegister();
            }
        }
    }

    private boolean rcValidate() {
        if (txtRcExpiry.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Registration Expiry");
            return false;
        }
        if (imgRc.getTag() == null) {
            CommonFunctions.shortToast(this, "Upload an Image");
            return false;
        }
        return true;
    }

    private void apiRegister() {
        HashMap<String, RequestBody> map = new HashMap<>();
        List<MultipartBody.Part> imageMap = new ArrayList<>();
        //params
        map.put("driver_name", WebUtils.createRequest(edtDriverName.getText().toString()));
        map.put("email", WebUtils.createRequest(edtDriverEmail.getText().toString()));
        map.put("phone_no", WebUtils.createRequest(ccp.getSelectedCountryCode() + edtDriverMobile.getText().toString()));
        map.put("password", WebUtils.createRequest(edtDriverPassword.getText().toString()));
        map.put("address_line_1", WebUtils.createRequest("0"));
        map.put("phone_brand", WebUtils.createRequest(txtBrand.getText().toString()));
        map.put("phone_model", WebUtils.createRequest(txtModel.getText().toString()));
        map.put("state_province", WebUtils.createRequest("0"));
        map.put("country", WebUtils.createRequest(countryResponseList.get(spinCountry.getSelectedItemPosition()).getCountry_id()));
        map.put("city", WebUtils.createRequest("0"));
        map.put("delivery_mode", WebUtils.createRequest("" + selectedDeliveryMode));
        map.put("country_code", WebUtils.createRequest(ccp.getSelectedCountryCode()));
        map.put("person_number", WebUtils.createRequest(txt_person_number.getText().toString()));
        if (selectedDeliveryMode > 1) {
            map.put("vehicle_model", WebUtils.createRequest(edtVehicleModel.getText().toString()));
            map.put("registration_number", WebUtils.createRequest(edtVehicleNo.getText().toString()));
        }
//

//        if (spinArea.getSelectedItem() == null || spinArea.getSelectedItem().toString().equalsIgnoreCase("Select Apt/Suite")) {
//            map.put("area", WebUtils.createRequest(" "));
//        } else {
//            map.put("area", WebUtils.createRequest(areaResponseList.get(spinArea.getSelectedItemPosition()).getId()));
//        }

        map.put("zip_code", WebUtils.createRequest("0"));
        //Profile Pic Upload
        if (imgProfile.getTag().toString().contains("http")) {
            map.put("profile_pic", WebUtils.createRequest(""));
        } else {
            imageMap.add(WebUtils.getImagePart("profile_pic", new File(imgProfile.getTag().toString())));
        }
        //RIGHT TO WORK
        map.put("right_to_work", WebUtils.createRequest(spinDocType.getSelectedItem().toString()));
        map.put("right_to_work_expiry_date", WebUtils.createRequest(txtWorkRightExpiry.getText().toString()));
        //image
        if (imgWorkRight.getTag().toString().contains("http")) {
            map.put("right_to_work_doc", WebUtils.createRequest(""));
        } else {
            imageMap.add(WebUtils.getImagePart("right_to_work_doc", new File(imgWorkRight.getTag().toString())));
        }
        //----------------------------------
        if (layDocInfo.getVisibility() == View.VISIBLE) {
            //removed for skal app
            /*map.put("ctp_number", WebUtils.createRequest(edtCtpNo.getText().toString()));
            map.put("ctp_expiry_date", WebUtils.createRequest(txtCtcExpiry.getText().toString()));
            map.put("registration_number", WebUtils.createRequest(edtRegNo.getText().toString()));
            map.put("registration_expiry_date", WebUtils.createRequest(txtRcExpiry.getText().toString()));

            if (imgCtc.getTag().toString().contains("http")) {
                map.put("ctp_doc", WebUtils.createRequest(""));
            } else {
                imageMap.add(WebUtils.getImagePart("ctp_doc", new File(imgCtc.getTag().toString())));
            }

            if (imgRc.getTag().toString().contains("http")) {
                map.put("registration_doc", WebUtils.createRequest(""));
            } else {
                imageMap.add(WebUtils.getImagePart("registration_doc", new File(imgRc.getTag().toString())));
            }*/

            map.put("license_expiry", WebUtils.createRequest(txtLicenseExpiry.getText().toString()));

            if (imgDriverLicense.getTag().toString().contains("http")) {
                map.put("license", WebUtils.createRequest(""));
            } else {
                imageMap.add(WebUtils.getImagePart("license", new File(imgDriverLicense.getTag().toString())));
            }


        } else {

            //removed for skal app
           /* map.put("ctp_number", WebUtils.createRequest(" "));
            map.put("registration_number", WebUtils.createRequest(" "));
            map.put("ctp_expiry_date", WebUtils.createRequest(" "));
            map.put("registration_expiry_date", WebUtils.createRequest(" "));

            map.put("ctp_doc", WebUtils.createRequest(""));
            map.put("registration_doc", WebUtils.createRequest(""));*/

            map.put("license_expiry", WebUtils.createRequest(" "));
            //image
            map.put("license", WebUtils.createRequest(""));
        }
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
        updatedata(map, imageMap);
//        System.out.println("00000000000000000000" + map + imageMap);
//        System.out.println("00000000000000000000" + imageMap);
//
//        System.out.println(edtDriverName.getText().toString());
//        System.out.println(edtDriverEmail.getText().toString());
//        System.out.println(ccp.getSelectedCountryCode() + edtDriverMobile.getText().toString());
//        System.out.println(edtDriverPassword.getText().toString());
//        System.out.println(edtAddress.getText().toString());
//        System.out.println(txtBrand.getText().toString());
//        System.out.println(txtModel.getText().toString());
//        map.put("state_province", WebUtils.createRequest("0"));
////        map.put("state_province", WebUtils.createRequest(stateResponseList.get(spinStateProvince.getSelectedItemPosition()).getState_id()));
//        System.out.println(countryResponseList.get(spinCountry.getSelectedItemPosition()).getCountry_id());
//        System.out.println(edt_area.getText().toString());
//        System.out.println("" + selectedDeliveryMode);
//        System.out.println(ccp.getSelectedCountryCode());
//        System.out.println(txt_person_number.getText().toString());


    }

    private void updatedata(HashMap<String, RequestBody> map, List<MultipartBody.Part> imageMap) {
        CommonFunctions.showSimpleProgressDialog(this, "Updating", false);
        Call<DriverRegisterResponse> call = apiInterface.registerDriver(sessionManager.getHeader(), map, imageMap);
        call.enqueue(new Callback<DriverRegisterResponse>() {
            @Override
            public void onResponse(Call<DriverRegisterResponse> call, Response<DriverRegisterResponse> response) {
                if (response.code() == 200) {

                    System.out.println("the response is:" + response.body().getStatus());
                    System.out.println("the response is:" + response.code());
                    System.out.println("the response is:" + response.body().getMessage());
                    System.out.println("the response is:" + map);

                    CommonFunctions.shortToast(RegisterActivity.this, "Driver Added Successfully");
                    finish();
                }
                CommonFunctions.removeProgressDialog();
            }

            @Override
            public void onFailure(Call<DriverRegisterResponse> call, Throwable t) {
                Log.e("TAG", "onFailure: updateBusinessApi" + t);
                CommonFunctions.removeProgressDialog();
            }
        });


    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setBackClickVisibility() {
        if (lay1.getVisibility() == View.VISIBLE) {
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (lay2.getVisibility() == View.VISIBLE) {
            progressBar.setProgress(25);
            lay3.setVisibility(View.GONE);
            lay2.setVisibility(View.GONE);
            lay1.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
        } else if (lay3.getVisibility() == View.VISIBLE) {
            progressBar.setProgress(50);
            lay4.setVisibility(View.GONE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.GONE);
            lay1.setVisibility(View.VISIBLE);
            lay5.setVisibility(View.GONE);
        } else if (lay4.getVisibility() == View.VISIBLE) {
            lay4.setVisibility(View.GONE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.VISIBLE);
            lay1.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
        } else if (lay5.getVisibility() == View.VISIBLE) {
            btnNext.setText("Next");
            txtTerms.setVisibility(View.GONE);
            lay4.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.GONE);
            lay1.setVisibility(View.GONE);
            lay5.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        setBackClickVisibility();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                setBackClickVisibility();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean ctcValidate() {
        if (txtCtcExpiry.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter CTC Expiry");
            return false;
        }
        if (imgCtc.getTag() == null) {
            CommonFunctions.shortToast(this, "Upload an Image");
            return false;
        }
        return true;
    }

    private boolean licenseValidate() {
        if (txtLicenseExpiry.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter License Expiry");
            txtLicenseExpiry.clearFocus();
            txtLicenseExpiry.requestFocus();
            return false;
        }
        if (imgDriverLicense.getTag() == null) {
            CommonFunctions.shortToast(this, "Upload an Image");
            return false;
        }
        return true;
    }

    private boolean workRightValidate() {
        if (spinDocType.getSelectedItem() == null) {
            CommonFunctions.shortToast(this, "Select a Document Type");
            return false;
        }
        if (txtWorkRightExpiry.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Work Expiry");
            return false;
        }
        if (imgWorkRight.getTag() == null) {
            CommonFunctions.shortToast(this, "Upload an Image");
            return false;
        }
        return true;
    }

    private boolean fieldValidate() {
        Log.e(RegisterActivity.class.getName(), "fieldValidate:spinCountry.getSelectedItem() " + spinCountry.getSelectedItem());
        Log.e(RegisterActivity.class.getName(), "fieldValidate:spinCountry.getSelectedItem() " + spinStateProvince.getSelectedItem());
//        Log.e(RegisterActivity.class.getName(), "fieldValidate:spinCountry.getSelectedItem() " + spinCity.getSelectedItem());
//        Log.e(RegisterActivity.class.getName(), "fieldValidate:spinCountry.getSelectedItem() " + spinArea.getSelectedItem());

        if (edtDriverName.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Name");
            edtDriverName.clearFocus();
            edtDriverName.requestFocus();
            return false;
        } else if (edtDriverPassword.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Password");
            edtDriverPassword.clearFocus();
            edtDriverPassword.requestFocus();
            return false;
        } else if (imgProfile.getTag() == null || imgProfile.getTag().toString().isEmpty()) {
            CommonFunctions.shortToast(this, getString(R.string.upload_profile_image));
            return false;
        } else if (edtDriverEmail.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(edtDriverEmail.getText().toString()).matches()) {
            CommonFunctions.shortToast(this, "Enter Email");
            edtDriverEmail.clearFocus();
            edtDriverEmail.requestFocus();
            return false;
        } else if (edtDriverMobile.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Mobile");
            edtDriverMobile.clearFocus();
            edtDriverMobile.requestFocus();
            return false;
        }
//        else if (edtAddress.getText().toString().isEmpty()) {
//            CommonFunctions.shortToast(this, "Enter Address");
//            edtAddress.clearFocus();
//            edtAddress.requestFocus();
//
//            return false;
//        } else if (edtZipcode.getText().toString().isEmpty()) {
//            CommonFunctions.shortToast(this, "Enter Zipcode");
//            edtZipcode.clearFocus();
//            edtZipcode.requestFocus();
//            return false;
//        }
        else if (spinCountry.getSelectedItem() == null || spinCountry.getSelectedItem().toString().equalsIgnoreCase("Select Country")) {
            CommonFunctions.shortToast(this, "Select a Country");
            return false;
        } else if (spinStateProvince.getSelectedItem() == null || spinStateProvince.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
            CommonFunctions.shortToast(this, "Select a City");
            return false;
        }
//        else if (spinCity.getSelectedItem() == null || spinCity.getSelectedItem().toString().equalsIgnoreCase("Select City")) {
//            CommonFunctions.shortToast(this, "Select a City");
//            return false;
//
//        }
//        else if (edt_area.getText().toString().isEmpty()) {
//            CommonFunctions.shortToast(this, "Enter Apt/Suite");
//            edt_area.clearFocus();
//            edt_area.requestFocus();
//            return false;
//        }
       /*else if (layDocInfo.getVisibility() == View.VISIBLE && edtRegNo.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter Register Number");
            return false;
        } else if (layDocInfo.getVisibility() == View.VISIBLE && edtCtpNo.getText().toString().isEmpty()) {
            CommonFunctions.shortToast(this, "Enter CTP Number");
            return false;
        }*/

       /* else if (spinArea.getSelectedItem() == null || spinArea.getSelectedItem().toString().equalsIgnoreCase("Select Area")) {
            CommonFunctions.shortToast(this, "Select a Area");
            return false;
        }
       */

        if (selectedDeliveryMode > 1) {
            if (edtVehicleModel.getText().toString().isEmpty()) {
                CommonFunctions.shortToast(this, "Enter Vehicle Model");
                return false;
            } else if (edtVehicleNo.getText().toString().isEmpty()) {
                CommonFunctions.shortToast(this, "Enter Vehicle No");
                return false;
            }
        }

        return true;
    }

    @Override
    public void onDateComplete(Date date) {
        String dateString = Global.setDateFormat(date, "dd/MM/yyyy");
        if (dateDialogFragment.getTag().equals("License Picker")) {
            txtLicenseExpiry.setText(dateString);
            Log.e("dateString", "onDateComplete:License " + dateString);
        } else if (dateDialogFragment.getTag().equals("CTC Picker")) {
            txtCtcExpiry.setText(dateString);
            Log.e("dateString", "onDateComplete:CTC " + dateString);
        } else if (dateDialogFragment.getTag().equals("Work Picker")) {
            txtWorkRightExpiry.setText(dateString);
            Log.e("dateString", "onDateComplete:Work " + dateString);
        } else if (dateDialogFragment.getTag().equals("RC Picker")) {
            txtRcExpiry.setText(dateString);
            Log.e("dateString", "onDateComplete:RC " + dateString);
        }

//        Global.setDate(dateString, "dd/MM/yyyy", "yyyy-MM-dd");

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        int id = adapterView.getId();
//        edtDriverEmail.setFocusable(false);
//        edtDriverName.setFocusable(false);
//        edtDriverMobile.setFocusable(false);
//        edtDriverPassword.setFocusable(false);
//        txt_person_number.setFocusable(false);
        switch (id) {
            case R.id.spin_country:

                if (spinCountry.getSelectedItemPosition() != 0) {
                    setDefaults(STATE);

                }
                break;
            case R.id.spin_state_province:
                if (spinStateProvince.getSelectedItemPosition() != 0) {
                    setDefaults(CITY);
                }
                break;
            case R.id.spin_city:
                if (spinCity.getSelectedItemPosition() != 0) {
                    setDefaults(AREA);
                }
                break;
            case R.id.spin_area:
                break;
            case R.id.spin_doc_type:
                break;
            case R.id.spin_deliv_mode:
                setDeliveryModeVisibility(spinDelivMode.getSelectedItem().toString());
                break;
        }
    }

    private void setDeliveryModeVisibility(String DeliveryMode) {
        switch (DeliveryMode) {
            case "Cycle":
                selectedDeliveryMode = 1;
                layDocInfo.setVisibility(View.GONE);
                documentLayout.setVisibility(View.GONE);
                break;
            case "Bike":
                selectedDeliveryMode = 2;
                layDocInfo.setVisibility(View.VISIBLE);
                documentLayout.setVisibility(View.VISIBLE);
                break;
            case "Car":
                selectedDeliveryMode = 3;
                layDocInfo.setVisibility(View.VISIBLE);
                documentLayout.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public int getCountryPosition(String countryName) {
        int position = 0;
        for (int i = 0; i < countryResponseList.size(); i++) {
            String name = countryResponseList.get(i).getCountry_name();
            System.out.println(name);
            if (countryResponseList.get(i).getCountry_name().equals(countryName)) {
                position = i;
                break;
            }
        }
        return position;
    }
}
