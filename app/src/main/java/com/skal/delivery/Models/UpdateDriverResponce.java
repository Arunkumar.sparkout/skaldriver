package com.skal.delivery.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skal.delivery.Profiledata;

public class UpdateDriverResponce {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Profiledata data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Profiledata getData() {
        return data;
    }

    public void setData(Profiledata data) {
        this.data = data;
    }

}

