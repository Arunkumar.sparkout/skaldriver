package com.skal.delivery.profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.skal.delivery.Common.CommonFunctions;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Config;
import com.skal.delivery.EventModels.LogoutEvent;
import com.skal.delivery.Models.ProfileDetailsResponse;
import com.skal.delivery.R;
import com.skal.delivery.SharedPrefsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skal.delivery.Common.SessionManager.KEY_USER_MOBILE;

public class Profilepage extends Fragment {
    //    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.txt_username)
    AppCompatTextView txtUsername;
    @BindView(R.id.txt_address)
    AppCompatTextView txtAddress;
    @BindView(R.id.txt_city)
    AppCompatTextView txtCity;
    @BindView(R.id.txt_rating)
    AppCompatTextView txtRating;
    @BindView(R.id.txt_phone)
    AppCompatTextView txtPhone;
    @BindView(R.id.txt_license_no)
    AppCompatTextView txtLicenseNo;
    @BindView(R.id.txt_join_date)
    AppCompatTextView txtJoinDate;
    @BindView(R.id.txt_serv_zone)
    AppCompatTextView txtServZone;
    @BindView(R.id.txt_bank_name)
    AppCompatTextView txtBankName;
    @BindView(R.id.txt_acc_no)
    AppCompatTextView txtAccNo;
    @BindView(R.id.txt_ifsc)
    AppCompatTextView txtIfsc;
    private APIInterface apiInterface;
    private SessionManager sessionManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sessionManager = new SessionManager(getContext());
        Log.e("Giri ", "userprofileicon: " + SharedPrefsUtils.getFromPrefs(getActivity(), Config.userprofileicon, ""));
        Glide.with(getActivity())
                .load(SharedPrefsUtils.getFromPrefs(getActivity(), Config.userprofileicon, ""))
                .into(imgProfile);
        try {
            txtUsername.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.username, ""));
            txtAddress.setText(getString(R.string.locality) + SharedPrefsUtils.getFromPrefs(getActivity(), Config.useraddressprofile, ""));
            txtCity.setText(getString(R.string.city) + SharedPrefsUtils.getFromPrefs(getActivity(), Config.userlocationprofile, ""));
            txtRating.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.userratingprofile, ""));
            txtPhone.setText(sessionManager.getUserDetails().get(KEY_USER_MOBILE));
            txtLicenseNo.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.usernameprofile, ""));
            txtJoinDate.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.userjoiningprofile, ""));
            txtServZone.setText(SharedPrefsUtils.getFromPrefs(getActivity(), Config.userservicezone, ""));
        } catch (Exception e) {
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String profileoncecall = SharedPrefsUtils.getFromPrefs(getActivity(), Config.profileoncecall, "");
        if (profileoncecall.equalsIgnoreCase("profilecallone")) {
            getProfileDetails();
        }
    }

    private void getProfileDetails() {
        Call<ProfileDetailsResponse> call = apiInterface.getProfile(sessionManager.getHeader(), sessionManager.getCurrentLanguage());
        System.out.println("Output" + sessionManager.getHeader());
        System.out.println("Output" + sessionManager.getCurrentLanguage());
        call.enqueue(new Callback<ProfileDetailsResponse>() {
            @Override
            public void onResponse(Call<ProfileDetailsResponse> call, Response<ProfileDetailsResponse> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus()) {
                        setDetails(response.body());
                    }
                } else if (response.code() == 401) {
                    sessionManager.logoutUser(getActivity());
                    CommonFunctions.shortToast(getActivity(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ProfileDetailsResponse> call, Throwable t) {
            }
        });
    }

    private void setDetails(ProfileDetailsResponse profileResponse) {
        try {
            txtUsername.setText(profileResponse.getName() + "(" + profileResponse.getPartner_id() + ")");
            txtAddress.setText(getString(R.string.locality) + profileResponse.getAddress());
            txtCity.setText(getString(R.string.city) + profileResponse.getCity());
            txtRating.setText(profileResponse.getRating());
            txtPhone.setText(profileResponse.getPhone());
            txtLicenseNo.setText(profileResponse.getDriving_license_no());
            txtJoinDate.setText(profileResponse.getJoining_date());
            txtServZone.setText(profileResponse.getService_zone());
            txtBankName.setText(profileResponse.getBank_name());
            txtAccNo.setText(profileResponse.getAcc_no());
            txtIfsc.setText(profileResponse.getIfsc_code());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.username, profileResponse.getName() + "(" + profileResponse.getPartner_id() + ")");
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.user, profileResponse.getName());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.useremail, profileResponse.getEmail());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.phonenumber, profileResponse.getPhone());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.usernameid, profileResponse.getId());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.usernameprofile, profileResponse.getName());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.useraddressprofile, profileResponse.getAddress());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.userratingprofile, profileResponse.getRating());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.userlocationprofile, profileResponse.getCity());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.userjoiningprofile, profileResponse.getJoining_date());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.userservicezone, profileResponse.getService_zone());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.userprofileicon, profileResponse.getProfile_pic());
            SharedPrefsUtils.saveToPrefs(getActivity(), Config.profileoncecall, "profilecallsecound");
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUnauthorise(LogoutEvent logoutEvent) {
        Log.e("tag", "onUnauthorise: Event");
        sessionManager.logoutUser(getActivity());
    }
}
