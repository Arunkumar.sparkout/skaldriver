package com.skal.delivery.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.skal.delivery.R;

import com.skal.delivery.profile.ProfileEditpage;
import com.skal.delivery.profile.Profilepage;
import com.theartofdev.edmodo.cropper.CropImage;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Boolean backpresscheck = false;
    FragmentTransaction transaction;
    Fragment fragmentA;
    ProfileEditpage fragmentB;
    FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        // loadFragmentOne(view);
        // toolbar.setTitle(getString(R.string.txt_about_us));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentA = fragmentManager.findFragmentByTag("frag1");

        if (fragmentA == null) {
            fragmentA = new Profilepage();
        }
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.framelayout, fragmentA, "frag1");
        transaction.commit();
    }

    public void edit(View v) {
        backpresscheck = true;
        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentB = fragmentManager.findFragmentByTag("frag2");
        if (fragmentB == null) {
            fragmentB = new ProfileEditpage();
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.framelayout, fragmentB, "frag2");
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN); //setting animation for fragment transaction
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (backpresscheck == true) {
            Log.e("1111", "Arun");
        } else {
            Log.e("1111", "kumar");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                if (null != fragmentB) {
                    fragmentB.getCropData(result.getUri());
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.e("Krish", "CropError: " + error.getMessage());
            }
        }
    }
}