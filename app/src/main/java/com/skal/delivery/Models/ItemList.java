package com.skal.delivery.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ItemList implements Parcelable {

    private String food_name;

    private String item_price;

    private String is_veg;

    private String food_quantity;

    private List<AddOns> add_ons;

    private String tax;

    private String food_id;

    private List<FoodQuantity> food_size;


    protected ItemList(Parcel in) {
        food_name = in.readString();
        item_price = in.readString();
        is_veg = in.readString();
        food_quantity = in.readString();
        add_ons = in.createTypedArrayList(AddOns.CREATOR);
        tax = in.readString();
        food_id = in.readString();
        food_size = in.createTypedArrayList(FoodQuantity.CREATOR);
    }

    public static final Creator<ItemList> CREATOR = new Creator<ItemList>() {
        @Override
        public ItemList createFromParcel(Parcel in) {
            return new ItemList(in);
        }

        @Override
        public ItemList[] newArray(int size) {
            return new ItemList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(food_name);
        parcel.writeString(item_price);
        parcel.writeString(is_veg);
        parcel.writeString(food_quantity);
        parcel.writeTypedList(add_ons);
        parcel.writeString(tax);
        parcel.writeString(food_id);
        parcel.writeTypedList(food_size);
    }

    public String getFood_name() {
        return food_name;
    }

    public void setFood_name(String food_name) {
        this.food_name = food_name;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getIs_veg() {
        return is_veg;
    }

    public void setIs_veg(String is_veg) {
        this.is_veg = is_veg;
    }

    public String getFood_quantity() {
        return food_quantity;
    }

    public void setFood_quantity(String food_quantity) {
        this.food_quantity = food_quantity;
    }

    public List<AddOns> getAdd_ons() {
        return add_ons;
    }

    public void setAdd_ons(List<AddOns> add_ons) {
        this.add_ons = add_ons;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getFood_id() {
        return food_id;
    }

    public void setFood_id(String food_id) {
        this.food_id = food_id;
    }

    public List<FoodQuantity> getFood_size() {
        return food_size;
    }

    public void setFood_size(List<FoodQuantity> food_size) {
        this.food_size = food_size;
    }
}
