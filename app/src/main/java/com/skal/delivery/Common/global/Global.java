package com.skal.delivery.Common.global;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import com.hbb20.CCPCountry;
import com.skal.delivery.Common.webService.APIClient;
import com.skal.delivery.Common.webService.APIInterface;
import com.skal.delivery.Models.DirectionResults;
import com.skal.delivery.Common.CONST;
import com.skal.delivery.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Global {
    private APIInterface apiService;
    Polyline line;
    List<DirectionResults.Routes> routesDetails = new ArrayList<>();
    List<LatLng> route = new ArrayList<>();
    private ArrayList<DirectionResults.Legs> Legs = new ArrayList<>();
    private ArrayList<DirectionResults.Steps> Steps = new ArrayList<>();


    public static double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    //set spannable text for registration signup policy text
    /*public static void setSpannableText(String Viewtext, final Activity activity, AppCompatTextView txtPolicy) {
        String beforeText = "By clicking "+ Viewtext;
        int length = beforeText.length();
        SpannableString ss = new SpannableString(beforeText+" you agree to Skal Delivery Driver Terms of Use and acknowledge you have read the Privacy Policy");
        Log.e("TAG", "setSpannableText: "+ss.toString().indexOf("Terms"));

        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //remove underline in text
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://178.128.27.246/eatzilla/cms/driver_termsandcondition"));
                activity.startActivity(browserIntent);

            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //remove underline in text
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://178.128.27.246/eatzilla/cms/driver_privacypolicy"));
                activity.startActivity(browserIntent);

            }
        };

        ss.setSpan(span1, length+39, length+52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, length+86, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        
        ss.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorAccent)), length+39, length+52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorAccent)), length+86, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtPolicy.setText(ss);
        txtPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }*/

    public static void setSpannableText(String Viewtext, final Activity activity, AppCompatTextView txtPolicy) {
        String termDesc = activity.getString(R.string.by_continue);
        String termDesc1 = activity.getString(R.string.ack_read);


        SpannableString ss = new SpannableString(activity.getString(R.string.terms_of_use));
        SpannableString ss1 = new SpannableString(activity.getString(R.string.privacy_police));

        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //remove underline in text
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(CONST.TERMS_CONDITIONS));
                activity.startActivity(browserIntent);

            }
        };

        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //remove underline in text
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(CONST.PRIVACY_POLICY));
                activity.startActivity(browserIntent);

            }
        };

        ss.setSpan(span1, 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(span2, 0, ss1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.blue)), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.blue)), 0, ss1.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtPolicy.setText(TextUtils.concat(termDesc, " ", ss, " ", termDesc1, " ", ss1));
        txtPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static Date getCurrentDate() {
        Date date = Calendar.getInstance().getTime();
        return date;
    }

    public static List<String> getCodes(String selectedCountryCode) {
        List<String> codeList = new ArrayList<>();
        codeList.add("MRZ");
        switch (selectedCountryCode) {
            case "USA":
                codeList.add("ARIDL");
                codeList.add("NEYDLF");
                break;
            case "GBR":
                codeList.add("UKDR");
                break;
            case "ARE":
                codeList.add("ENIDF");
                break;
            case "TUR":
                codeList.add("TUIDF");
                break;
            case "ESP":
                codeList.add("SPIDF");
                break;
            case "SGP":
                codeList.add("SINID");
                break;
            case "ROU":
                codeList.add("RONID");
                break;
            case "NLD":
                codeList.add("NETIDF");
                break;
            case "MYS":
                codeList.add("MALID");
                break;
            case "KWT":
                codeList.add("KUWIDF");
                break;
            case "IND":
                codeList.add("PAN71");
                codeList.add("ADHF");
                break;
            case "HKG":
                codeList.add("HKPID");
                codeList.add("HKPEID");
                break;
            case "BGD":
                codeList.add("OLNAID");
                codeList.add("NENAIDF");
                break;
            case "AUS":
                codeList.add("AUNID");
                codeList.add("NSWDL");
                break;
            default:
                break;
        }
        return codeList;
    }

    public static String getCodeName(String code) {
        String name = "";
        switch (code) {
            case "MRZ":
                name = "Passport/ID";
                break;
            case "ARIDL":
                name = "Arizona Driving License";
                break;
            case "NEYDLF":
                name = "New York Driving License";
                break;
            case "UKDR":
                name = "UK Driving Licence";
                break;
            case "ENIDF":
                name = "Emirates National ID";
                break;
            case "TUIDF":
                name = "Turkey Identity Card";
                break;
            case "SPIDF":
                name = "Spanish Id Card";
                break;
            case "SINID":
                name = "Singapore ID Card";
                break;
            case "RONID":
                name = "Romania ID Card";
                break;
            case "NETIDF":
                name = "Netherlands ID card";
                break;
            case "MALID":
                name = "Malaysia My KAD ID";
                break;
            case "KUWIDF":
                name = "Kuwait Civil ID Card";
                break;
            case "PAN71":
                name = "Pan Card";
                break;
            case "ADHF":
                name = "Aadhar Card Front";
                break;
            case "HKPID":
                name = "HK Permanent ID 1";
                break;
            case "HKPEID":
                name = "HK Permanent ID 2";
                break;
            case "OLNAID":
                name = "Old National ID Card";
                break;
            case "NENAIDF":
                name = "New National ID Card";
                break;
            case "AUNID":
                name = "Victoria Driving License";
                break;
            case "NSWDL":
                name = "New South Wales Driver Licence";
                break;
        }
        return name;
    }

    /*public static List<CCPCountry> getLibraryMasterCountriesEnglish() {
        user.put("ad", "AND", "387", "Andorra", DEFAULT_FLAG_RES));
        user.put("ae", "ARE", "387", "United Arab Emirates (UAE)", DEFAULT_FLAG_RES));
        user.put("af", "AFG", "387", "Afghanistan", DEFAULT_FLAG_RES));
        user.put("ag", "ATG", "387", "Antigua and Barbuda", DEFAULT_FLAG_RES));
        user.put("ai", "ai", "387", "Anguilla", DEFAULT_FLAG_RES));
        user.put("al", "ALB", "387", "Albania", DEFAULT_FLAG_RES));
        user.put("am", "ARM", "387", "Armenia", DEFAULT_FLAG_RES));
        user.put("ao", "AGO", "387", "Angola", DEFAULT_FLAG_RES));
        user.put("aq", "aq", "387", "Antarctica", DEFAULT_FLAG_RES));
        user.put("ar", "ARG", "387", "Argentina", DEFAULT_FLAG_RES));
        user.put("as", "as", "387", "American Samoa", DEFAULT_FLAG_RES));
        user.put("at", "AUT", "387", "Austria", DEFAULT_FLAG_RES));
        user.put("au", "AUS", "387", "Australia", DEFAULT_FLAG_RES));
        user.put("aw", "aw", "387", "Aruba", DEFAULT_FLAG_RES));
        user.put("ax", "ax", "387", "Åland Islands", DEFAULT_FLAG_RES));
        user.put("az", "AZE", "387", "Azerbaijan", DEFAULT_FLAG_RES));

        user.put("ba", "BIH", "387", "Bosnia And Herzegovina", DEFAULT_FLAG_RES));
        user.put("bb", "BRB", "1", "Barbados", DEFAULT_FLAG_RES));
        user.put("bd", "BGD", "880", "Bangladesh", DEFAULT_FLAG_RES));
        user.put("be", "BEL", "32", "Belgium", DEFAULT_FLAG_RES));
        user.put("bf", "BFA", "226", "Burkina Faso", DEFAULT_FLAG_RES));
        user.put("bg", "BGR", "359", "Bulgaria", DEFAULT_FLAG_RES));
        user.put("bh", "BHR", "973", "Bahrain", DEFAULT_FLAG_RES));
        user.put("bi", "BDI", "257", "Burundi", DEFAULT_FLAG_RES));
        user.put("bj", "BEN", "229", "Benin", DEFAULT_FLAG_RES));
        user.put("bl", "bl", "590", "Saint Barthélemy", DEFAULT_FLAG_RES));
        user.put("bm", "bm", "1", "Bermuda", DEFAULT_FLAG_RES));
        user.put("bn", "bn", "673", "Brunei Darussalam", DEFAULT_FLAG_RES));
        user.put("bo", "bo", "591", "Bolivia, Plurinational State Of", DEFAULT_FLAG_RES));
        user.put("br", "BRA", "55", "Brazil", DEFAULT_FLAG_RES));
        user.put("bs", "BHS", "1", "Bahamas", DEFAULT_FLAG_RES));
        user.put("bt", "BTN", "975", "Bhutan", DEFAULT_FLAG_RES));
        user.put("bw", "BWA", "267", "Botswana", DEFAULT_FLAG_RES));
        user.put("by", "BLR", "375", "Belarus", DEFAULT_FLAG_RES));
        user.put("bz", "BLZ", "501", "Belize", DEFAULT_FLAG_RES));
        user.put("ca", "CAN", "1", "Canada", DEFAULT_FLAG_RES));
        user.put("cc", "cc", "61", "Cocos (keeling) Islands", DEFAULT_FLAG_RES));
        user.put("cd", "COD", "243", "Congo, The Democratic Republic Of The", DEFAULT_FLAG_RES));
        user.put("cf", "CAF", "236", "Central African Republic", DEFAULT_FLAG_RES));
        user.put("cg", "COG", "242", "Congo", DEFAULT_FLAG_RES));
        user.put("ch", "CHE", "41", "Switzerland", DEFAULT_FLAG_RES));
        user.put("ci", "ci", "225", "Côte D'ivoire", DEFAULT_FLAG_RES));
        user.put("ck", "ck", "682", "Cook Islands", DEFAULT_FLAG_RES));
        user.put("cl", "CHL", "56", "Chile", DEFAULT_FLAG_RES));
        user.put("cm", "CMR", "237", "Cameroon", DEFAULT_FLAG_RES));
        user.put("cn", "CHN", "86", "China", DEFAULT_FLAG_RES));
        user.put("co", "COL", "57", "Colombia", DEFAULT_FLAG_RES));
        user.put("cr", "CRI", "506", "Costa Rica", DEFAULT_FLAG_RES));
        user.put("cu", "CUB", "53", "Cuba", DEFAULT_FLAG_RES));
        user.put("cv", "cv", "238", "Cape Verde", DEFAULT_FLAG_RES));
        user.put("cw", "cw", "599", "Curaçao", DEFAULT_FLAG_RES));
        user.put("cx", "cx", "61", "Christmas Island", DEFAULT_FLAG_RES));
        user.put("cy", "CYP", "357", "Cyprus", DEFAULT_FLAG_RES));
        user.put("cz", "cz", "420", "Czech Republic", DEFAULT_FLAG_RES));
        user.put("de", "DEU", "49", "Germany", DEFAULT_FLAG_RES));
        user.put("dj", "DJI", "253", "Djibouti", DEFAULT_FLAG_RES));
        user.put("dk", "DNK", "45", "Denmark", DEFAULT_FLAG_RES));
        user.put("dm", "DMA", "1", "Dominica", DEFAULT_FLAG_RES));
        user.put("do", "DOM", "1", "Dominican Republic", DEFAULT_FLAG_RES));
        user.put("dz", "DZA", "213", "Algeria", DEFAULT_FLAG_RES));
        user.put("ec", "ECU", "593", "Ecuador", DEFAULT_FLAG_RES));
        user.put("ee", "EST", "372", "Estonia", DEFAULT_FLAG_RES));
        user.put("eg", "EGY", "20", "Egypt", DEFAULT_FLAG_RES));
        user.put("er", "ERI", "291", "Eritrea", DEFAULT_FLAG_RES));
        user.put("es", "ESP", "34", "Spain", DEFAULT_FLAG_RES));
        user.put("et", "ETH", "251", "Ethiopia", DEFAULT_FLAG_RES));
        user.put("fi", "FIN", "358", "Finland", DEFAULT_FLAG_RES));
        user.put("fj", "FJI", "679", "Fiji", DEFAULT_FLAG_RES));
        user.put("fk", "fk", "500", "Falkland Islands (malvinas)", DEFAULT_FLAG_RES));
        user.put("fm", "FSM", "691", "Micronesia, Federated States Of", DEFAULT_FLAG_RES));
        user.put("fo", "fo", "298", "Faroe Islands", DEFAULT_FLAG_RES));
        user.put("fr", "FRA", "33", "France", DEFAULT_FLAG_RES));
        user.put("ga", "GAB", "241", "Gabon", DEFAULT_FLAG_RES));
        user.put("gb", "GBR", "44", "United Kingdom", DEFAULT_FLAG_RES));
        user.put("gd", "GRD", "1", "Grenada", DEFAULT_FLAG_RES));
        user.put("ge", "GMB", "995", "Georgia", DEFAULT_FLAG_RES));
        user.put("gf", "gf", "594", "French Guyana", DEFAULT_FLAG_RES));
        user.put("gh", "GHA", "233", "Ghana", DEFAULT_FLAG_RES));
        user.put("gi", "gi", "350", "Gibraltar", DEFAULT_FLAG_RES));
        user.put("gl", "gl", "299", "Greenland", DEFAULT_FLAG_RES));
        user.put("gm", "GMB", "220", "Gambia", DEFAULT_FLAG_RES));
        user.put("gn", "GIN", "224", "Guinea", DEFAULT_FLAG_RES));
        user.put("gp", "gp", "450", "Guadeloupe", DEFAULT_FLAG_RES));
        user.put("gq", "GNQ", "240", "Equatorial Guinea", DEFAULT_FLAG_RES));
        user.put("gr", "GRC", "30", "Greece", DEFAULT_FLAG_RES));
        user.put("gt", "GTM", "502", "Guatemala", DEFAULT_FLAG_RES));
        user.put("gu", "gu", "1", "Guam", DEFAULT_FLAG_RES));
        user.put("gw", "gw", "245", "Guinea-bissau", DEFAULT_FLAG_RES));
        user.put("gy", "GUY", "592", "Guyana", DEFAULT_FLAG_RES));
        user.put("hk", "HKG", "852", "Hong Kong", DEFAULT_FLAG_RES));
        user.put("hn", "HND", "504", "Honduras", DEFAULT_FLAG_RES));
        user.put("hr", "HRV", "385", "Croatia", DEFAULT_FLAG_RES));
        user.put("ht", "HTI", "509", "Haiti", DEFAULT_FLAG_RES));
        user.put("hu", "HUN", "36", "Hungary", DEFAULT_FLAG_RES));
        user.put("id", "IDN", "62", "Indonesia", DEFAULT_FLAG_RES));
        user.put("ie", "IRL", "353", "Ireland", DEFAULT_FLAG_RES));
        user.put("il", "ISR", "972", "Israel", DEFAULT_FLAG_RES));
        user.put("im", "im", "44", "Isle Of Man", DEFAULT_FLAG_RES));
        user.put("is", "ISL", "354", "Iceland", DEFAULT_FLAG_RES));
        user.put("in", "IND", "91", "India", DEFAULT_FLAG_RES));
        user.put("io", "io", "246", "British Indian Ocean Territory", DEFAULT_FLAG_RES));
        user.put("iq", "IRQ", "964", "Iraq", DEFAULT_FLAG_RES));
        user.put("ir", "IRN", "98", "Iran, Islamic Republic Of", DEFAULT_FLAG_RES));
        user.put("it", "ITA", "39", "Italy", DEFAULT_FLAG_RES));
        user.put("je", "je", "44", "Jersey ", DEFAULT_FLAG_RES));
        user.put("jm", "JAM", "1", "Jamaica", DEFAULT_FLAG_RES));
        user.put("jo", "JOR", "962", "Jordan", DEFAULT_FLAG_RES));
        user.put("jp", "JPN", "81", "Japan", DEFAULT_FLAG_RES));
        user.put("ke", "KEN", "254", "Kenya", DEFAULT_FLAG_RES));
        user.put("kg", "KGZ", "996", "Kyrgyzstan", DEFAULT_FLAG_RES));
        user.put("kh", "KHM", "855", "Cambodia", DEFAULT_FLAG_RES));
        user.put("ki", "KIR", "686", "Kiribati", DEFAULT_FLAG_RES));
        user.put("km", "COM", "269", "Comoros", DEFAULT_FLAG_RES));
        user.put("kn", "KNA", "1", "Saint Kitts and Nevis", DEFAULT_FLAG_RES));
        user.put("kp", "PRK", "850", "North Korea", DEFAULT_FLAG_RES));
        user.put("kr", "KOR", "82", "South Korea", DEFAULT_FLAG_RES));
        user.put("kw", "KWT", "965", "Kuwait", DEFAULT_FLAG_RES));
        user.put("ky", "ky", "1", "Cayman Islands", DEFAULT_FLAG_RES));
        user.put("kz", "KAZ", "7", "Kazakhstan", DEFAULT_FLAG_RES));
        user.put("la", "la", "856", "Lao People's Democratic Republic", DEFAULT_FLAG_RES));
        user.put("lb", "LBN", "961", "Lebanon", DEFAULT_FLAG_RES));
        user.put("lc", "LCA", "1", "Saint Lucia", DEFAULT_FLAG_RES));
        user.put("li", "LIE", "423", "Liechtenstein", DEFAULT_FLAG_RES));
        user.put("lk", "LKA", "94", "Sri Lanka", DEFAULT_FLAG_RES));
        user.put("lr", "LBR", "231", "Liberia", DEFAULT_FLAG_RES));
        user.put("ls", "LSO", "266", "Lesotho", DEFAULT_FLAG_RES));
        user.put("lt", "LTU", "370", "Lithuania", DEFAULT_FLAG_RES));
        user.put("lu", "LUX", "352", "Luxembourg", DEFAULT_FLAG_RES));
        user.put("lv", "LVA", "371", "Latvia", DEFAULT_FLAG_RES));
        user.put("ly", "LBY", "218", "Libya", DEFAULT_FLAG_RES));
        user.put("ma", "MAR", "212", "Morocco", DEFAULT_FLAG_RES));
        user.put("mc", "MCO", "377", "Monaco", DEFAULT_FLAG_RES));
        user.put("md", "MDA", "373", "Moldova, Republic Of", DEFAULT_FLAG_RES));
        user.put("me", "MNE", "382", "Montenegro", DEFAULT_FLAG_RES));
        user.put("mf", "mf", "590", "Saint Martin", DEFAULT_FLAG_RES));
        user.put("mg", "MDG", "261", "Madagascar", DEFAULT_FLAG_RES));
        user.put("mh", "MHL", "692", "Marshall Islands", DEFAULT_FLAG_RES));
        user.put("mk", "MKD", "389", "Macedonia (FYROM)", DEFAULT_FLAG_RES));
        user.put("ml", "MLI", "223", "Mali", DEFAULT_FLAG_RES));
        user.put("mm", "MMR", "95", "Myanmar", DEFAULT_FLAG_RES));
        user.put("mn", "MNG", "976", "Mongolia", DEFAULT_FLAG_RES));
        user.put("mo", "mo", "853", "Macau", DEFAULT_FLAG_RES));
        user.put("mp", "mp", "1", "Northern Mariana Islands", DEFAULT_FLAG_RES));
        user.put("mq", "mq", "596", "Martinique", DEFAULT_FLAG_RES));
        user.put("mr", "MRT", "222", "Mauritania", DEFAULT_FLAG_RES));
        user.put("ms", "ms", "1", "Montserrat", DEFAULT_FLAG_RES));
        user.put("mt", "MLT", "356", "Malta", DEFAULT_FLAG_RES));
        user.put("mu", "MUS", "230", "Mauritius", DEFAULT_FLAG_RES));
        user.put("mv", "MDV", "960", "Maldives", DEFAULT_FLAG_RES));
        user.put("mw", "MWI", "265", "Malawi", DEFAULT_FLAG_RES));
        user.put("mx", "MEX", "52", "Mexico", DEFAULT_FLAG_RES));
        user.put("my", "MYS", "60", "Malaysia", DEFAULT_FLAG_RES));
        user.put("mz", "MOZ", "258", "Mozambique", DEFAULT_FLAG_RES));
        user.put("na", "NAM", "264", "Namibia", DEFAULT_FLAG_RES));
        user.put("nc", "nc", "687", "New Caledonia", DEFAULT_FLAG_RES));
        user.put("ne", "NER", "227", "Niger", DEFAULT_FLAG_RES));
        user.put("nf", "nf", "672", "Norfolk Islands", DEFAULT_FLAG_RES));
        user.put("ng", "NGA", "234", "Nigeria", DEFAULT_FLAG_RES));
        user.put("ni", "NIC", "505", "Nicaragua", DEFAULT_FLAG_RES));
        user.put("nl", "nl", "31", "Netherlands", DEFAULT_FLAG_RES));
        user.put("no", "NOR", "47", "Norway", DEFAULT_FLAG_RES));
        user.put("np", "NPL", "977", "Nepal", DEFAULT_FLAG_RES));
        user.put("nr", "NRU", "674", "Nauru", DEFAULT_FLAG_RES));
        user.put("nu", "nu", "683", "Niue", DEFAULT_FLAG_RES));
        user.put("nz", "NZL", "64", "New Zealand", DEFAULT_FLAG_RES));
        user.put("om", "OMN", "968", "Oman", DEFAULT_FLAG_RES));
        user.put("pa", "PAN", "507", "Panama", DEFAULT_FLAG_RES));
        user.put("pe", "PER", "51", "Peru", DEFAULT_FLAG_RES));
        user.put("pf", "pf", "689", "French Polynesia", DEFAULT_FLAG_RES));
        user.put("pg", "PNG", "675", "Papua New Guinea", DEFAULT_FLAG_RES));
        user.put("ph", "PHL", "63", "Philippines", DEFAULT_FLAG_RES));
        user.put("pk", "PAK", "92", "Pakistan", DEFAULT_FLAG_RES));
        user.put("pl", "POL", "48", "Poland", DEFAULT_FLAG_RES));
        user.put("pm", "pm", "508", "Saint Pierre And Miquelon", DEFAULT_FLAG_RES));
        user.put("pn", "pn", "870", "Pitcairn Islands", DEFAULT_FLAG_RES));
        user.put("pr", "pr", "1", "Puerto Rico", DEFAULT_FLAG_RES));
        user.put("ps", "PSE", "970", "Palestine", DEFAULT_FLAG_RES));
        user.put("pt", "PRT", "351", "Portugal", DEFAULT_FLAG_RES));
        user.put("pw", "PLW", "680", "Palau", DEFAULT_FLAG_RES));
        user.put("py", "PRY", "595", "Paraguay", DEFAULT_FLAG_RES));
        user.put("qa", "QAT", "974", "Qatar", DEFAULT_FLAG_RES));
        user.put("re", "re", "262", "Réunion", DEFAULT_FLAG_RES));
        user.put("ro", "ROU", "40", "Romania", DEFAULT_FLAG_RES));
        user.put("rs", "SRB", "381", "Serbia", DEFAULT_FLAG_RES));
        user.put("ru", "RUS", "7", "Russian Federation", DEFAULT_FLAG_RES));
        user.put("rw", "RWA", "250", "Rwanda", DEFAULT_FLAG_RES));
        user.put("sa", "SAU", "966", "Saudi Arabia", DEFAULT_FLAG_RES));
        user.put("sb", "SLB", "677", "Solomon Islands", DEFAULT_FLAG_RES));
        user.put("sc", "SYC", "248", "Seychelles", DEFAULT_FLAG_RES));
        user.put("sd", "SDN", "249", "Sudan", DEFAULT_FLAG_RES));
        user.put("se", "SWE", "46", "Sweden", DEFAULT_FLAG_RES));
        user.put("sg", "SGP", "65", "Singapore", DEFAULT_FLAG_RES));
        user.put("sh", "sh", "290", "Saint Helena, Ascension And Tristan Da Cunha", DEFAULT_FLAG_RES))
        ;
        user.put("si", "SVN", "386", "Slovenia", DEFAULT_FLAG_RES));
        user.put("sk", "SVK", "421", "Slovakia", DEFAULT_FLAG_RES));
        user.put("sl", "SLE", "232", "Sierra Leone", DEFAULT_FLAG_RES));
        user.put("sm", "SMR", "378", "San Marino", DEFAULT_FLAG_RES));
        user.put("sn", "sn", "221", "Senegal", DEFAULT_FLAG_RES));
        user.put("so", "SOM", "252", "Somalia", DEFAULT_FLAG_RES));
        user.put("sr", "SUR", "597", "Suriname", DEFAULT_FLAG_RES));
        user.put("ss", "SSD", "211", "South Sudan", DEFAULT_FLAG_RES));
        user.put("st", "STP", "239", "Sao Tome And Principe", DEFAULT_FLAG_RES));
        user.put("sv", "SLV", "503", "El Salvador", DEFAULT_FLAG_RES));
        user.put("sx", "sx", "1", "Sint Maarten", DEFAULT_FLAG_RES));
        user.put("sy", "SYR", "963", "Syrian Arab Republic", DEFAULT_FLAG_RES));
        user.put("sz", "sz", "268", "Swaziland", DEFAULT_FLAG_RES));
        user.put("tc", "tc", "1", "Turks and Caicos Islands", DEFAULT_FLAG_RES));
        user.put("td", "TCD", "235", "Chad", DEFAULT_FLAG_RES));
        user.put("tg", "TGO", "228", "Togo", DEFAULT_FLAG_RES));
        user.put("th", "THA", "66", "Thailand", DEFAULT_FLAG_RES));
        user.put("tj", "TJK", "992", "Tajikistan", DEFAULT_FLAG_RES));
        user.put("tk", "tk", "690", "Tokelau", DEFAULT_FLAG_RES));
        user.put("tl", "tl", "670", "Timor-leste", DEFAULT_FLAG_RES));
        user.put("tm", "TKM", "993", "Turkmenistan", DEFAULT_FLAG_RES));
        user.put("tn", "TUN", "216", "Tunisia", DEFAULT_FLAG_RES));
        user.put("to", "TON", "676", "Tonga", DEFAULT_FLAG_RES));
        user.put("tr", "TUR", "90", "Turkey", DEFAULT_FLAG_RES));
        user.put("tt", "TTO", "1", "Trinidad &amp; Tobago", DEFAULT_FLAG_RES));
        user.put("tv", "TUV", "688", "Tuvalu", DEFAULT_FLAG_RES));
        user.put("tw", "TWN", "886", "Taiwan", DEFAULT_FLAG_RES));
        user.put("tz", "TZA", "255", "Tanzania, United Republic Of", DEFAULT_FLAG_RES));
        user.put("ua", "UKR", "380", "Ukraine", DEFAULT_FLAG_RES));
        user.put("ug", "UGA", "256", "Uganda", DEFAULT_FLAG_RES));
        user.put("us", "USA", "1", "United States", DEFAULT_FLAG_RES));
        user.put("uy", "URY", "598", "Uruguay", DEFAULT_FLAG_RES));
        user.put("uz", "UZB", "998", "Uzbekistan", DEFAULT_FLAG_RES));
        user.put("va", "VAT", "379", "Holy See (vatican City State)", DEFAULT_FLAG_RES));
        user.put("vc", "VCT", "1", "Saint Vincent &amp; The Grenadines", DEFAULT_FLAG_RES));
        user.put("ve", "VEN", "58", "Venezuela, Bolivarian Republic Of", DEFAULT_FLAG_RES));
        user.put("vg", "vg", "1", "British Virgin Islands", DEFAULT_FLAG_RES));
        user.put("vi", "vi", "1", "US Virgin Islands", DEFAULT_FLAG_RES));
        user.put("vn", "VNM", "84", "Vietnam", DEFAULT_FLAG_RES));
        user.put("vu", "VUT", "678", "Vanuatu", DEFAULT_FLAG_RES));
        user.put("wf", "wf", "681", "Wallis And Futuna", DEFAULT_FLAG_RES));
        user.put("ws", "WSM", "685", "Samoa", DEFAULT_FLAG_RES));
        user.put("xk", "XKX", "383", "Kosovo", DEFAULT_FLAG_RES));
        user.put("ye", "YEM", "967", "Yemen", DEFAULT_FLAG_RES));
        user.put("yt", "yt", "262", "Mayotte", DEFAULT_FLAG_RES));
        user.put("za", "ZAF", "27", "South Africa", DEFAULT_FLAG_RES));
        user.put("zm", "ZMB", "260", "Zambia", DEFAULT_FLAG_RES));
        user.put("zw", "ZWE", "263", "Zimbabwe", DEFAULT_FLAG_RES));
        return countries;
    }*/

    public HashMap<String, String> setCountryCode() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("ad", "AND");
        user.put("ae", "ARE");
        user.put("af", "AFG");
        user.put("ag", "ATG");
        user.put("ar", "ARG");
        user.put("al", "ALB");
        user.put("am", "ARM");
        user.put("ao", "AGO");
        user.put("at", "AUT");
        user.put("au", "AUS");
        user.put("az", "AZE");
        user.put("ba", "BIH");
        user.put("bb", "BRB");
        user.put("bd", "BGD");
        user.put("be", "BEL");
        user.put("bf", "BFA");
        user.put("bg", "BGR");
        user.put("bh", "BHR");
        user.put("bi", "BDI");
        user.put("bj", "BEN");
        user.put("br", "BRA");
        user.put("bs", "BHS");
        user.put("bt", "BTN");
        user.put("bw", "BWA");
        user.put("by", "BLR");
        user.put("bz", "BLZ");
        user.put("ca", "CAN");
        user.put("cd", "COD");
        user.put("cf", "CAF");
        user.put("cg", "COG");
        user.put("ch", "CHE");
        user.put("cl", "CHL");
        user.put("cm", "CMR");
        user.put("cn", "CHN");
        user.put("co", "COL");
        user.put("cr", "CRI");
        user.put("cu", "CUB");
        user.put("cy", "CYP");
        user.put("de", "DEU");
        user.put("dj", "DJI");
        user.put("dk", "DNK");
        user.put("dm", "DMA");
        user.put("do", "DOM");
        user.put("dz", "DZA");
        user.put("ec", "ECU");
        user.put("ee", "EST");
        user.put("eg", "EGY");
        user.put("er", "ERI");
        user.put("es", "ESP");
        user.put("et", "ETH");
        user.put("fi", "FIN");
        user.put("fj", "FJI");
        user.put("fm", "FSM");
        user.put("fr", "FRA");
        user.put("ga", "GAB");
        user.put("gb", "GBR");
        user.put("gd", "GRD");
        user.put("ge", "GMB");
        user.put("gh", "GHA");
        user.put("gm", "GMB");
        user.put("gn", "GIN");
        user.put("gq", "GNQ");
        user.put("gr", "GRC");
        user.put("gt", "GTM");
        user.put("gy", "GUY");
        user.put("hk", "HKG");
        user.put("hn", "HND");
        user.put("hr", "HRV");
        user.put("ht", "HTI");
        user.put("hu", "HUN");
        user.put("id", "IDN");
        user.put("ie", "IRL");
        user.put("il", "ISR");
        user.put("is", "ISL");
        user.put("in", "IND");
        user.put("iq", "IRQ");
        user.put("ir", "IRN");
        user.put("it", "ITA");
        user.put("jm", "JAM");
        user.put("jo", "JOR");
        user.put("jp", "JPN");
        user.put("ke", "KEN");
        user.put("kg", "KGZ");
        user.put("kh", "KHM");
        user.put("ki", "KIR");
        user.put("km", "COM");
        user.put("kn", "KNA");
        user.put("kp", "PRK");
        user.put("kr", "KOR");
        user.put("kw", "KWT");
        user.put("kz", "KAZ");
        user.put("lb", "LBN");
        user.put("lc", "LCA");
        user.put("li", "LIE");
        user.put("lk", "LKA");
        user.put("lr", "LBR");
        user.put("ls", "LSO");
        user.put("lt", "LTU");
        user.put("lu", "LUX");
        user.put("lv", "LVA");
        user.put("ly", "LBY");
        user.put("ma", "MAR");
        user.put("mc", "MCO");
        user.put("md", "MDA");
        user.put("me", "MNE");
        user.put("mg", "MDG");
        user.put("mh", "MHL");
        user.put("mk", "MKD");
        user.put("ml", "MLI");
        user.put("mm", "MMR");
        user.put("mn", "MNG");
        user.put("mr", "MRT");
        user.put("mt", "MLT");
        user.put("mu", "MUS");
        user.put("mv", "MDV");
        user.put("mw", "MWI");
        user.put("mx", "MEX");
        user.put("my", "MYS");
        user.put("mz", "MOZ");
        user.put("na", "NAM");
        user.put("ne", "NER");
        user.put("ng", "NGA");
        user.put("ni", "NIC");
        user.put("no", "NOR");
        user.put("np", "NPL");
        user.put("nr", "NRU");
        user.put("nz", "NZL");
        user.put("om", "OMN");
        user.put("pa", "PAN");
        user.put("pe", "PER");
        user.put("pg", "PNG");
        user.put("ph", "PHL");
        user.put("pk", "PAK");
        user.put("pl", "POL");
        user.put("ps", "PSE");
        user.put("pt", "PRT");
        user.put("pw", "PLW");
        user.put("py", "PRY");
        user.put("qa", "QAT");
        user.put("ro", "ROU");
        user.put("rs", "SRB");
        user.put("ru", "RUS");
        user.put("rw", "RWA");
        user.put("sa", "SAU");
        user.put("sb", "SLB");
        user.put("sc", "SYC");
        user.put("sd", "SDN");
        user.put("se", "SWE");
        user.put("sg", "SGP");
        user.put("si", "SVN");
        user.put("sk", "SVK");
        user.put("sl", "SLE");
        user.put("sm", "SMR");
        user.put("so", "SOM");
        user.put("sr", "SUR");
        user.put("ss", "SSD");
        user.put("st", "STP");
        user.put("sv", "SLV");
        user.put("sy", "SYR");
        user.put("td", "TCD");
        user.put("tg", "TGO");
        user.put("th", "THA");
        user.put("tj", "TJK");
        user.put("tm", "TKM");
        user.put("tn", "TUN");
        user.put("to", "TON");
        user.put("tr", "TUR");
        user.put("tt", "TTO");
        user.put("tv", "TUV");
        user.put("tw", "TWN");
        user.put("tz", "TZA");
        user.put("ua", "UKR");
        user.put("ug", "UGA");
        user.put("us", "USA");
        user.put("uy", "URY");
        user.put("uz", "UZB");
        user.put("va", "VAT");
        user.put("vc", "VCT");
        user.put("ve", "VEN");
        user.put("vn", "VNM");
        user.put("vu", "VUT");
        user.put("ws", "WSM");
        user.put("xk", "XKX");
        user.put("ye", "YEM");
        user.put("za", "ZAF");
        user.put("zm", "ZMB");
        user.put("zw", "ZWE");
        user.put("nl", "NLD");

        user.put("ai", "ai");
        user.put("aq", "aq");
        user.put("as", "as");
        user.put("aw", "aw");
        user.put("ax", "ax");
        user.put("im", "im");
        user.put("gf", "gf");
        user.put("fo", "fo");
        user.put("fk", "fk");
        user.put("cv", "cv");
        user.put("cw", "cw");
        user.put("cx", "cx");
        user.put("bl", "bl");
        user.put("bm", "bm");
        user.put("bn", "bn");
        user.put("bo", "bo");
        user.put("cc", "cc");
        user.put("ci", "ci");
        user.put("ck", "ck");
        user.put("cz", "cz");
        user.put("gi", "gi");
        user.put("gl", "gl");
        user.put("gp", "gp");
        user.put("gu", "gu");
        user.put("gw", "gw");
        user.put("io", "io");
        user.put("je", "je");
        user.put("ky", "ky");
        user.put("la", "la");
        user.put("mf", "mf");
        user.put("mo", "mo");
        user.put("mp", "mp");
        user.put("mq", "mq");
        user.put("ms", "ms");
        user.put("nc", "nc");
        user.put("nf", "nf");
        user.put("nu", "nu");
        user.put("pf", "pf");
        user.put("pm", "pm");
        user.put("pn", "pn");
        user.put("pr", "pr");
        user.put("re", "re");
        user.put("sh", "sh");
        user.put("sn", "sn");
        user.put("sx", "sx");
        user.put("tk", "tk");
        user.put("tl", "tl");
        user.put("sz", "sz");
        user.put("tc", "tc");
        user.put("vg", "vg");
        user.put("vi", "vi");
        user.put("wf", "wf");
        user.put("yt", "yt");

        // return user
        return user;
    }

    public static String setDateFormat(Date date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
        String datePicked = sdf.format(date.getTime());
        return datePicked;
    }

    public static String getDateFromString(String dateString, String perviousformat, String newFormat) {
        DateFormat oldDateFormat = new SimpleDateFormat(perviousformat, Locale.ENGLISH);
        DateFormat newDateFormat = new SimpleDateFormat(newFormat, Locale.ENGLISH);
        Date olddate = null;
        String newDate = null;
        try {
            olddate = oldDateFormat.parse(dateString);
            newDate = newDateFormat.format(olddate);
        } catch (ParseException e) {
            Log.e("Global", "getDateFromString:ParseException " + e);
        }

        return newDate;
    }

    public static String getCurrentTime(String dateformat) {

        SimpleDateFormat df1 = new SimpleDateFormat(dateformat);

        String dateString = df1.format(new Date(System.currentTimeMillis()));

        return dateString;
    }

    public static int getTimeOutSeconds(String assigned_time, int notification_time) {

        //getting time difference between assigned time and current time
        //getting timeout seconds from their difference
        int finalTime;
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = outputFormat.parse(assigned_time);
            endDate = outputFormat.parse(getCurrentTime("yyyy-MM-dd HH:mm:ss"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diffInMs = endDate.getTime() - startDate.getTime();

        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);

        if ((notification_time - diffInSec) < 0) {
            finalTime = 0;
        } else {
            finalTime = (int) (notification_time - diffInSec);
        }

        /*long diffMs = endDate.getTime() - startDate.getTime();
        long diffSec = diffMs / 1000;
        long min = diffSec / 60;
        long sec = diffSec % 60;
        Log.e("TAG", "getTimeOutSeconds:finalTime "+"The difference is "+min+" minutes and "+sec+" seconds.");*/
        return finalTime;

    }

    public static String getLocationCountryCode(Activity activity, LatLng latLng) {
        String countryCode = null;

        Geocoder geocoder = new Geocoder(activity);

        try {
            List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                countryCode = addressList.get(0).getCountryCode().trim();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return countryCode;
    }

    public void directionJson(Context context, LatLng pickup, LatLng drop, GoogleMap mMap, boolean addAdditionalPolyline) {
        apiService = APIClient.getPlacesClient().create(APIInterface.class);
        if (pickup != null && drop != null) {
            final String origin = "" + pickup.latitude + "," + pickup.longitude;
            String destination = "" + drop.latitude + "," + drop.longitude;
            Call<DirectionResults> call = apiService.getRoute(CONST.APIKEY, origin, destination);
            call.enqueue(new Callback<DirectionResults>() {
                @Override
                public void onResponse(Call<DirectionResults> call, Response<DirectionResults> response) {
                    if (response.code() == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("OK")) {
                            if (line != null && !addAdditionalPolyline) {
                                line.remove();
                            }

                            routesDetails.clear();
                            route.clear();
                            routesDetails.addAll(response.body().getRoutes());

                            //get values using legs Value
                            parseData();

                            line = mMap.addPolyline(new PolylineOptions()
                                    .width(6)
                                    .color(context.getResources().getColor(R.color.green)));
                            line.setPoints(route);

                            //Zoom camera
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            builder.include(pickup).include(drop);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), 20);
                            mMap.animateCamera(cu);

                        }
                    }
                }

                @Override
                public void onFailure(Call<DirectionResults> call, Throwable t) {
                    // Log error here since request failed
                    Toast.makeText(context, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    System.out.println("Retro Error" + t.getMessage().toString());
                    call.cancel();
                }
            });

        }
    }

    private void parseData() {
        /** Traversing all routes */
        for (int i = 0; i < routesDetails.size(); i++) {
            Legs = routesDetails.get(i).getLegs();
            List path = new ArrayList<HashMap<String, String>>();

            /** Traversing all legs */
            for (int j = 0; j < Legs.size(); j++) {
                Steps = Legs.get(j).getSteps();

                /** Traversing all steps */
                for (int k = 0; k < Steps.size(); k++) {
                    String polyline = "";
                    polyline = Steps.get(k).getPolyline().getPoints();
                    List list = decodePoly(polyline);

                    /** Traversing all points */
                    for (int l = 0; l < list.size(); l++) {
                        Double lat = ((LatLng) list.get(l)).latitude;
                        Double lng = ((LatLng) list.get(l)).longitude;
                        LatLng latLng = new LatLng(lat, lng);
                        route.add(latLng);
                    }
                }
//                    route.add(path);
            }
        }
    }

    private List decodePoly(String encoded) {

        List poly = new ArrayList();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    public static String setDate(String oldDate, String oldFormat, String newFormat) {
        DateFormat outputFormat = new SimpleDateFormat(newFormat, java.util.Locale.getDefault());
        DateFormat inputFormat = new SimpleDateFormat(oldFormat, Locale.US);
        String finalDate = null;
        Date date = null;
        try {
            String input = oldDate;
            date = inputFormat.parse(input);
            finalDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return finalDate;
    }


    public boolean isMyServiceRunning(Class<?> serviceClass, Context context) {

        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service already", "running");
                return true;
            }
        }
        Log.i("Service not", "running");
        return false;
    }

    //set Black and grey color in same string
    public static String setMultipleColorText(String firstText, String secondText) {
        String text = "<font color=#c9adad>" + firstText + " " + "</font> <font color=#000000 ><b>" + secondText + "</b></font>";
        return text;
    }

}
