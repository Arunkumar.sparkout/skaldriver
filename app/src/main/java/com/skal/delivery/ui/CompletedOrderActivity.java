package com.skal.delivery.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skal.delivery.Common.CONST;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.global.Global;
import com.skal.delivery.EventModels.LogoutEvent;
import com.skal.delivery.Models.ItemList;
import com.skal.delivery.Models.PastOrders;
import com.skal.delivery.ui.Adapter.PastOrderItemAdapter;
import com.skal.delivery.R;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompletedOrderActivity extends AppCompatActivity {
    PastOrders pastOrders;
    List<ItemList> itemListList;
    @BindView(R.id.recycler_item_list)
    RecyclerView recyclerItemList;
    @BindView(R.id.item_total_amount_txt)
    TextView itemTotalAmountTxt;
    @BindView(R.id.offer_discount_amount_txt)
    TextView offerDiscountAmountTxt;
    @BindView(R.id.packaging_charge_amount_txt)
    TextView packagingChargeAmountTxt;
    @BindView(R.id.gst_amount_txt)
    TextView gstAmountTxt;
    @BindView(R.id.delivery_charge_amount_txt)
    TextView deliveryChargeAmountTxt;
    @BindView(R.id.total_to_pay_amount_txt)
    TextView totalToPayAmountTxt;
    @BindView(R.id.hotel_img)
    ImageView hotelImg;
    @BindView(R.id.hotel_name_txt)
    TextView hotelNameTxt;
    @BindView(R.id.hotel_place_txt)
    TextView hotelPlaceTxt;
    @BindView(R.id.hotel_delivery_txt)
    TextView hotelDeliveryTxt;
    @BindView(R.id.address_txt)
    TextView addressTxt;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.item_total_discount_txt)
    TextView itemTotalDiscountTxt;
    @BindView(R.id.driver_tip_txt)
    TextView driverTipTxt;
    @BindView(R.id.loyality_offer_txt)
    TextView loyalityOfferTxt;
    @BindView(R.id.delivery_tip_layout)
    RelativeLayout delivery_tip_layout;
    private PastOrderItemAdapter pastOrderItemAdapter;
    private SessionManager sessionManager;
    private String currencyStr;
    DecimalFormat precision = new DecimalFormat("0.00");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_order);
        ButterKnife.bind(this);


        sessionManager = new SessionManager(CompletedOrderActivity.this);
        currencyStr = sessionManager.getCurrency();

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        pastOrders = bundle.getParcelable(CONST.PAST_ORDER_DETAIL);
        itemListList = bundle.getParcelableArrayList(CONST.PAST_ORDER_ITEMS);
        setAdapter();
        setDetails();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {
        //set Toolbar
        String time = Global.getDateFromString(pastOrders.getOrdered_on(), "yyyy-MM-dd HH:mm", "hh:mm a");
        toolbar.setTitle(pastOrders.getOrder_id());
        toolbar.setSubtitle(time + " | " + itemListList.size() + " Item" + " | " + pastOrders.getBill_amount() + currencyStr);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //set Layout Manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerItemList.setLayoutManager(linearLayoutManager);
        recyclerItemList.setNestedScrollingEnabled(false);
        pastOrderItemAdapter = new PastOrderItemAdapter(CompletedOrderActivity.this, itemListList, currencyStr);
        recyclerItemList.setAdapter(pastOrderItemAdapter);
    }

    private void setDetails() {
        String dateTime = Global.getDateFromString(pastOrders.getOrdered_on(), "yyyy-MM-dd HH:mm", "EEEE MMM, d hh:mm a");
        Log.e("Giri ", "setDetails:setTitle " + pastOrders.getOrder_id());
        Picasso.get().load(pastOrders.getRestaurant_image()).into(hotelImg);
        hotelNameTxt.setText(pastOrders.getRestaurant_name());
        hotelPlaceTxt.setText(pastOrders.getRestaurant_address());
        hotelDeliveryTxt.setText(dateTime);
        addressTxt.setText(pastOrders.getDelivery_address());

        itemTotalAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getItem_total())) + " " + currencyStr);
        offerDiscountAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getOffer_discount())) + " " + currencyStr);
        packagingChargeAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getRestaurant_packaging_charge())) + " " + currencyStr);
        gstAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getTax())) + " " + currencyStr);
        deliveryChargeAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getDelivery_charge())) + " " + currencyStr);

//        itemTotalDiscountTxt.setText(precision.format(Double.parseDouble(pastOrders.getRestaurant_discount() + " " + currencyStr)));
        loyalityOfferTxt.setText(precision.format(Double.parseDouble(pastOrders.getRestaurant_discount())) + " " + currencyStr);
        driverTipTxt.setText(precision.format(Double.parseDouble(pastOrders.getDriver_tip())) + " " + currencyStr);
        System.out.println("00000000000000" + pastOrders.getStatus());

        if (pastOrders.getStatus().equalsIgnoreCase("7") ||
                pastOrders.getStatus().equalsIgnoreCase("8") ||
                pastOrders.getStatus().equalsIgnoreCase("9")) {
            totalToPayAmountTxt.setText(precision.format(Double.parseDouble(pastOrders.getBill_amount())) + " " + currencyStr);
            delivery_tip_layout.setVisibility(View.VISIBLE);
        } else {
            Double finalvalue = Double.valueOf(precision.format(Double.parseDouble(pastOrders.getBill_amount())))
                    -Double.valueOf(precision.format(Double.parseDouble(pastOrders.getDriver_tip())));
            totalToPayAmountTxt.setText(String.valueOf(finalvalue) + " " + currencyStr);
            delivery_tip_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUnauthorise(LogoutEvent logoutEvent) {
        Log.e("TAG", "onUnauthorise: Event");
        sessionManager.logoutUser(this);
    }
}
