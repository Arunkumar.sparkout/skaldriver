package com.skal.delivery.Models;

public class DriverRegistrationRequest {
    private String driver_name;
    private String email;
    private String phone_no;
    private String address_line_1;
    private String state_province;
    private String country;
    private String city;
    private String area;
    private String zip_code;
    private String license_expiry;
    private String password;
    private String registration_number;
    private String ctp_number;
    private String right_to_work;
    private String registration_expiry_date;
    private String ctp_expiry_date;
    private String right_to_work_expiry_date;
    private String profile_pic;
    private String license;
    private String registration_doc;
    private String ctp_doc;
    private String right_to_work_doc;

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getLicense_expiry() {
        return license_expiry;
    }

    public void setLicense_expiry(String license_expiry) {
        this.license_expiry = license_expiry;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getCtp_number() {
        return ctp_number;
    }

    public void setCtp_number(String ctp_number) {
        this.ctp_number = ctp_number;
    }

    public String getRight_to_work() {
        return right_to_work;
    }

    public void setRight_to_work(String right_to_work) {
        this.right_to_work = right_to_work;
    }

    public String getRegistration_expiry_date() {
        return registration_expiry_date;
    }

    public void setRegistration_expiry_date(String registration_expiry_date) {
        this.registration_expiry_date = registration_expiry_date;
    }

    public String getCtp_expiry_date() {
        return ctp_expiry_date;
    }

    public void setCtp_expiry_date(String ctp_expiry_date) {
        this.ctp_expiry_date = ctp_expiry_date;
    }

    public String getRight_to_work_expiry_date() {
        return right_to_work_expiry_date;
    }

    public void setRight_to_work_expiry_date(String right_to_work_expiry_date) {
        this.right_to_work_expiry_date = right_to_work_expiry_date;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getRegistration_doc() {
        return registration_doc;
    }

    public void setRegistration_doc(String registration_doc) {
        this.registration_doc = registration_doc;
    }

    public String getCtp_doc() {
        return ctp_doc;
    }

    public void setCtp_doc(String ctp_doc) {
        this.ctp_doc = ctp_doc;
    }

    public String getRight_to_work_doc() {
        return right_to_work_doc;
    }

    public void setRight_to_work_doc(String right_to_work_doc) {
        this.right_to_work_doc = right_to_work_doc;
    }
}
