package com.skal.delivery.ui.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ncorti.slidetoact.SlideToActView;
import com.skal.delivery.Common.CONST;
import com.skal.delivery.Common.SessionManager;
import com.skal.delivery.Common.global.Global;
import com.skal.delivery.Config;
import com.skal.delivery.Models.ItemList;
import com.skal.delivery.Models.PastOrders;
import com.bumptech.glide.Glide;
import com.skal.delivery.R;
import com.skal.delivery.ui.CompletedOrderActivity;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastOrderAdapter extends RecyclerView.Adapter<PastOrderAdapter.Viewholder> {
    Activity activity;
    List<PastOrders> pastOrders;
    List<ItemList> itemLists;
    private String ordersMenu;
    private final SessionManager sessionManager;


    public PastOrderAdapter(Activity activity, List<PastOrders> pastOrders) {
        Log.e("Giri ", "PastOrderAdapter:pastOrders " + pastOrders.size());
        this.activity = activity;
        this.pastOrders = pastOrders;
        sessionManager = new SessionManager(activity);

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_history_list, parent, false);
        return new Viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Log.e("Giri ", "onBindViewHolder: " + position);
        itemLists = pastOrders.get(position).getItem_list();
        Glide.with(activity)
                .load(pastOrders.get(position).getRestaurant_image())
                .into(holder.imgFood);
        holder.hotelName.setText("Order # " + pastOrders.get(position).getOrder_id());
//        holder.txtAmount.setText("$".concat(pastOrders.get(position).getBill_amount()));
        holder.txtAmount.setText(pastOrders.get(position).getBill_amount() + " " + sessionManager.getCurrency());
        holder.hotelLoc.setText(pastOrders.get(position).getRestaurant_address());
        String time = Global.getDateFromString(pastOrders.get(position).getOrdered_on(), "yyyy-MM-dd HH:mm", "EEEE MMM, d hh:mm a");
        holder.txtOrderno.setText(time);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < itemLists.size(); i++) {
            Log.e("TAG", "onBindViewHolder: Food_name" + itemLists.get(i).getFood_name());
            String itemName;
            if (i == itemLists.size() - 1) {
                itemName = itemLists.get(i).getFood_quantity() + " x " + itemLists.get(i).getFood_name() + ".";
            } else {
                itemName = itemLists.get(i).getFood_quantity() + " x " + itemLists.get(i).getFood_name() + ",";
            }
            ordersMenu = stringBuilder.append(itemName).toString();
            Log.e("TAG", "onBindViewHolder: ordersMenu" + ordersMenu);
        }
        holder.txtItems.setText(ordersMenu);
        String status = pastOrders.get(position).getStatus();
        if (status.equals("8")) {
            holder.order_return_status.setVisibility(View.VISIBLE);
        } else {
            holder.order_return_status.setVisibility(View.GONE);
        }
        if (status.equals("9")) {
            holder.order_return_status.setVisibility(View.VISIBLE);
            holder.slideToActView.setVisibility(View.GONE);
        } else {
            holder.order_return_status.setVisibility(View.GONE);
        }

        //System.out.println("00000000000000000"+status);
        holder.slideToActView.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(@NotNull SlideToActView slideToActView) {
                slideToActView.setBackgroundColor(Color.TRANSPARENT);

            }
        });
        ;
    }

    @Override
    public int getItemCount() {
        return pastOrders.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_food)
        AppCompatImageView imgFood;
        @BindView(R.id.hotel_name)
        AppCompatTextView hotelName;
        @BindView(R.id.hotel_loc)
        AppCompatTextView hotelLoc;
        @BindView(R.id.txt_items)
        AppCompatTextView txtItems;
        @BindView(R.id.txt_orderno)
        AppCompatTextView txtOrderno;
        @BindView(R.id.txt_amount)
        AppCompatTextView txtAmount;
        @BindView(R.id.example_slide_view)
        SlideToActView slideToActView;
        @BindView(R.id.order_return_status)
        LinearLayout order_return_status;

        public Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("Giri ", "onClick:itemView " + pastOrders.get(getAdapterPosition()).getItem_list().size());
                    Intent intent = new Intent(activity, CompletedOrderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(CONST.PAST_ORDER_DETAIL, pastOrders.get(getAdapterPosition()));
                    bundle.putParcelableArrayList(CONST.PAST_ORDER_ITEMS, (ArrayList<? extends Parcelable>) pastOrders.get(getAdapterPosition()).getItem_list());
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            });
        }
    }
}
